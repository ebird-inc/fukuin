<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "recruit",
		"css" => array(
			"basic",
			"recruit",
		),
		"title" => "採用情報｜株式会社フクイン",
		"description" => "株式会社フクインの採用情報のページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Recruit</p>
        <p>採用情報</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/recruit/">採用情報</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>FUKUINの採用</h1>
            <p>Recruitment informat 2017</p>
          </header>
          <nav class="jump">
            <ul>
              <li><a href="#member"><span>社員を知る</span></a></li>
              <li><a href="#style"><span>働き方について</span></a></li>
              <li><a href="#information"><span>フクインを知る</span></a></li>
            </ul>
          </nav>
					<div class="sidefull">
						<section id="intro">
							<figure>
								<div class="for-pc"><img src="../assets/images/recruit/intro_img.jpg" alt="「技術」を磨き、「人」を磨き、「印刷」の可能性を広げていく仲間を歓迎しています。"></div>
								<div class="for-sp"><img src="../assets/images/recruit/intro_img_sp.jpg" alt=""></div>							 
							</figure>
						</section>
					</div>
   	     	<section id="message">
  	 	     	<header>
							<h2>採用担当からのメッセージ</h2>
						 <p>Message</p>
						</header>
       			<p>近年のＩＴの進化により溢れだす情報を、適切に的確に表現し、<br>
						お客様に新たな価値を提供するのが私たちの使命と考えております。</p>
						<p>わたしたちは70年にわたり、何度も苦境を乗り越え変化し続けてきました。<br>
						めまぐるしく変わる世の中において、新しい価値を創造するには、<br>
						自分自身も常に変化し、成長しなければなりません。</p>
						<p>会社とともに、変化を恐れず、自らを磨き、成長していきたい人を歓迎しています。<br>
						わたしたちは中小企業ですが、その分、社員ひとりひとりが力を十分に発揮し、活躍できる会社です。<br>
						少しでもフクインに興味を持った方がいましたら、是非、お問い合わせください。</p>
						<p>最後に、就職活動は自分を見つめなおすいい機会である一方、たくさんの苦難もあると思います。</p>
						<p>苦難を乗り越え、皆さんが輝ける企業にきっと出会えることを願っております。</p>
		        <nav class="recruit">
			        <ul>
     				  	<li><a href="/recruit/fresh/">新卒 募集要項</a></li>
       					<li><a href="/recruit/career/">中途 募集要項</a></li>
       					<li><a href="/recruit/fresh/#fresh">会社説明会・面接会情報</a></li>
							</ul>
						</nav>
					</section>
          <section id="member">
          	<header>
							<h2>社員を知る</h2>
							<p>Member</p>
						</header>
						<p>部署も性格もまったく違う人間が協力をし、仕事に誇りを持ちながら日々前に進んでいます。<br>
							印刷という仕事を通して集まったフクインのスタッフ達をご紹介します。</p>
						<ul class="member">
							<li>
								<a href="" data-mfp-src="#member-01">
									<figure class="thumb"><span class="for-pc"><img src="../assets/images/recruit/member_01_thumb.jpg" alt=""></span><span class="for-sp"><img src="../assets/images/recruit/member_01_thumb_sp.jpg" alt=""></span></figure>
									<p class="dept">営業部　第二課</p>
									<p class="name">轟　淳奈</p>
									<p class="romaji">Junna Todoroki</p>
									<p class="year">2017年入社</p>
									<p class="more">詳細を見る</p>
								</a>
							</li>
							<li>
								<a href="" data-mfp-src="#member-02">
									<figure class="thumb"><span class="for-pc"><img src="../assets/images/recruit/member_02_thumb.jpg" alt=""></span><span class="for-sp"><img src="../assets/images/recruit/member_02_thumb_sp.jpg" alt=""></span></figure>
									<p class="dept">営業部　第二課</p>
									<p class="name">我謝 佑介</p>
									<p class="romaji">Yusuke Gaja</p>
									<p class="year">2008年入社</p>
									<p class="more">詳細を見る</p>
								</a>
							</li>
							<li>
								<a href="" data-mfp-src="#member-03">
									<figure class="thumb"><span class="for-pc"><img src="../assets/images/recruit/member_03_thumb.jpg" alt=""></span><span class="for-sp"><img src="../assets/images/recruit/member_03_thumb_sp.jpg" alt=""></span></figure>
									<p class="dept">生産統括部　印刷課</p>
									<p class="name">鬼頭 光司</p>
									<p class="romaji">Kouji Kitou</p>
									<p class="year">2016年入社</p>
									<p class="more">詳細を見る</p>
								</a>
							</li>
							<li>
								<a href="" data-mfp-src="#member-04">
									<figure class="thumb"><span class="for-pc"><img src="../assets/images/recruit/member_04_thumb.jpg" alt=""></span><span class="for-sp"><img src="../assets/images/recruit/member_04_thumb_sp.jpg" alt=""></span></figure>
									<p class="dept">生産統括部　印刷課</p>
									<p class="name">川上 瑞己</p>
									<p class="romaji">Mizuki Kawakami</p>
									<p class="year">2016年入社</p>
									<p class="more">詳細を見る</p>
								</a>
							</li>
							<li>
								<a href="" data-mfp-src="#member-05">
									<figure class="thumb"><span class="for-pc"><img src="../assets/images/recruit/member_05_thumb.jpg" alt=""></span><span class="for-sp"><img src="../assets/images/recruit/member_05_thumb_sp.jpg" alt=""></span></figure>
									<p class="dept">生産統括部　印刷課</p>
									<p class="name">吉野　俊</p>
									<p class="romaji">Shun Yoshino</p>
									<p class="year">2007年入社</p>
									<p class="more">詳細を見る</p>
								</a>
							</li>
							<li>
								<a href="" data-mfp-src="#member-06">
									<figure class="thumb"><span class="for-pc"><img src="../assets/images/recruit/member_06_thumb.jpg" alt=""></span><span class="for-sp"><img src="../assets/images/recruit/member_06_thumb_sp.jpg" alt=""></span></figure>
									<p class="dept">総務部　総務経理課</p>
									<p class="name">髙橋 玲美</p>
									<p class="romaji">Remi Takahashi</p>
									<p class="year">2016年入社</p>
									<p class="more">詳細を見る</p>
								</a>
							</li>
							<li>
								<a href="" data-mfp-src="#member-07">
									<figure class="thumb"><span class="for-pc"><img src="../assets/images/recruit/member_07_thumb.jpg" alt=""></span><span class="for-sp"><img src="../assets/images/recruit/member_07_thumb_sp.jpg" alt=""></span></figure>
									<p class="dept">営業部　営業管理課</p>
									<p class="name">山上 智美</p>
									<p class="romaji">Tomomi Yamagami</p>
									<p class="year">2007年入社</p>
									<p class="more">詳細を見る</p>
								</a>
							</li>
						</ul>
						<section id="member-01" class="member-dialog mfp-hide">
							<figure><img src="../assets/images/recruit/member_01_img.jpg" alt=""></figure>
							<div class="wrapper">
								<p class="dept">営業部　第二課</p>
								<p class="name">轟　淳奈</p>
								<p class="romaji">Junna Todoroki</p>
								<p class="year">2017年入社</p>
								<dl>
									<dt class="work">あなたの仕事の内容を簡単に教えてください。</dt>
									<dd>営業部で、出版社の一般書籍関係を主に担当しています。お客様に発注を受けてから、原稿が校了になるまでのやり取りを行います。</dd>
									<dt class="heart">仕事のやりがいはなんですか？　どんなことに達成感を感じますか？</dt>
									<dd>幼い頃から本が好きなので、好きなものが作り上げられていく過程に関われることが嬉しく、やりがいを感じます。また、使えなかった敬語を使えるようになったり、緊張せずに電話をとれるようになったり、少しずつ学生から社会人に成長していると思えた時、達成感があります。</dd>
									<dt class="problem">過去に「失敗したなぁ」という出来事はありますか？　又は、仕事のここが大変！というのはどんなところですか？</dt>
									<dd>複数の発注が重なった時は、受けている仕事の管理が出来ず、何をどこまでやったのか分からなくなってしまいました。その結果、手つかずの発注に気が付かず、周りの方々に迷惑をかけてしまいました。学生時代に社会人の先輩から、「予定の管理は大事だ」と聞いていましたが、その意味がやっと分かりました。</dd>
									<dt class="office">あなたの職場はどんな雰囲気ですか？</dt>
									<dd>誰にでも質問できる、温かい職場だと思います。入社して4ヶ月、毎日分からない事ばかりでしたが、皆さんがいつも質問に答えて下さるおかげで仕事も少しずつ覚えてきました。まだまだ頭がパンクしそうな時はありますが、部署を問わず先輩方が声をかけて下さるので心強いです。</dd>
									<dt class="message">一言メッセージ</dt>
									<dd>学生時代は不規則な生活をしていましたが、社会人になってから生活リズムが整い、ご飯が美味しくなりました！</dd>
								</dl>
							</div>
							<div class="close"><button class="mfp-close"></button></div>
						</section>
						<section id="member-02" class="member-dialog mfp-hide">
							<figure><img src="../assets/images/recruit/member_02_img.jpg" alt=""></figure>
							<div class="wrapper">
								<p class="dept">営業部　第二課</p>
								<p class="name">我謝 佑介</p>
								<p class="romaji">Yusuke Gaja</p>
								<p class="year">2008年入社</p>
								<dl>
									<dt class="work">あなたの仕事の内容を簡単に教えてください。</dt>
									<dd>出版関係の担当営業をしています。主な活動が海外生産の案件となる為、1年の3か月ほど海外へ出張に行っています。</dd>
									<dt class="heart">仕事のやりがいはなんですか？　どんなことに達成感を感じますか？</dt>
									<dd>営業職は、多くの人と触れ合える仕事です。会社の窓口として時には辛い時もありますが、お客様から依頼された難題を社員の人達やパートナー企業様と協力して達成できた時にとても遣り甲斐を感じます。何よりお客様より「フクインに仕事を出してよかった、我謝さんが担当でよかった」と言ってもらえた時は、今までの疲れが吹き飛び、次への活力になります。</dd>
									<dt class="problem">過去に「失敗したなぁ」という出来事はありますか？　又は、仕事のここが大変！というのはどんなところですか？</dt>
									<dd>海外での生産の時に、「言葉の壁」を感じました。私は営業5年目で海外生産の担当が2年目になります。日本での常識が海外での常識とイコールではなく伝えたつもりが相手に伝わっておらず、失敗をしてしまったことがあります。海外では主に通訳さんを通して話をしているので直接担当の方とコミュニケーションがとりづらい為、伝えたつもりになるのはとても怖いことだと感じました。これは仕事に限らず、日々人と接する時には、相手にどう伝えたいのかということが大事かなと思います。相手の目線に立ち物事を考えないといけないなと感じる出来事でした。</dd>
									<dt class="office">あなたの職場はどんな雰囲気ですか？</dt>
									<dd>印刷業は、人の手間がとてもかかります。廻りが忙しい時などは、言葉を掛け合い協力しあうことが多い為、コミュニケーションが取りやすい環境です。</dd>
									<dt class="message">一言メッセージ</dt>
									<dd>私は、現在営業部ですが、製本工程の現場など色々な経験をさせて頂いております。どの仕事を始める上でも初めはストレスを感じます。しかしそれを達成したときに遣り甲斐を感じます。どの仕事にも共通しておりますが、自分自身の仕事の楽しみ方が重要かなと思います。</dd>
								</dl>
							</div>
							<div class="close"><button class="mfp-close"></button></div>
						</section>
						<section id="member-03" class="member-dialog mfp-hide">
							<figure><img src="../assets/images/recruit/member_03_img.jpg" alt=""></figure>
							<div class="wrapper">
									<p class="dept">生産統括部　印刷課</p>
									<p class="name">鬼頭 光司</p>
									<p class="romaji">Kouji Kitou</p>
									<p class="year">2016年入社</p>							
								<dl>
									<dt class="work">あなたの仕事の内容を簡単に教えてください。</dt>
									<dd>機長の補助。印刷を開始するにあたり、用紙、インキ、版などの準備。印刷が終わった後の出荷検査、機械清掃等。</dd>
									<dt class="heart">仕事のやりがいはなんですか？　どんなことに達成感を感じますか？</dt>
									<dd>用紙が順調に流れ、印刷機の流れを止めない事です。用紙を流す為の調整が難しく、サイズや厚さ、クセなど用紙によって違います。それらの用紙を経験、失敗する時があっても次に繋がっていく事に達成感を感じます。</dd>
									<dt class="problem">過去に「失敗したなぁ」という出来事はありますか？　又は、仕事のここが大変！というのはどんなところですか？</dt>
									<dd>全く用紙が流れない、印刷機に通らない時は冷や汗がでます。また、調整がうまくいかずに用紙が破けた時は、紙片を探すのに苦労します。</dd>
									<dt class="office">あなたの職場はどんな雰囲気ですか？</dt>
									<dd>実力主義というか、一人一人が毎日完璧な仕事をしています。事故やミスが起きないように、緊張感のある職場だと思います。そんな中でも、和やかな会話もあり良い意味で緊張感が緩みます。</dd>
									<dt class="message">一言メッセージ</dt>
									<dd>大変な仕事ですが、自分が携わった本やチラシ、ポスターなどが世の中に出てその印刷物を誰かが見て、何かを感じてくれると嬉しいです。これが印刷の醍醐味だと思います。一緒に印刷、もの作りをしましょう。</dd>
								</dl>
							</div>
							<div class="close"><button class="mfp-close"></button></div>
						</section>
						<section id="member-04" class="member-dialog mfp-hide">
							<figure><img src="../assets/images/recruit/member_04_img.jpg" alt=""></figure>
							<div class="wrapper">
									<p class="dept">生産統括部　印刷課</p>
									<p class="name">川上 瑞己</p>
									<p class="romaji">Mizuki Kawakami</p>
									<p class="year">2016年入社</p>
								<dl>
									<dt class="work">あなたの仕事の内容を簡単に教えてください。</dt>
									<dd>用紙を積んだり、色調を見たり、印刷機の操作、管理などをしています。</dd>
									<dt class="heart">仕事のやりがいはなんですか？　どんなことに達成感を感じますか？</dt>
									<dd>印刷物を一番最初にみられる事です。見本と同じになるよう色調などを合わせ、その通りに印刷物が上がった時に達成感を感じます。</dd>
									<dt class="problem">過去に「失敗したなぁ」という出来事はありますか？　又は、仕事のここが大変！というのはどんなところですか？</dt>
									<dd>仕様書を見間違えて印刷してしまった事です。確認作業の大切さを実感しました。体力仕事ですので男性社員との力の差を感じる事がありますが、その差を埋める努力を日々行っています。</dd>
									<dt class="office">あなたの職場はどんな雰囲気ですか？</dt>
									<dd>仕事の合間に冗談などを言ったりするような、和やかな雰囲気です。ですが、仕事中は真剣に作業に取り組んでいてメリハリのある職場です。</dd>
									<dt class="message">一言メッセージ</dt>
									<dd>印刷は重い物を運んだり、時間に追われたりなど大変な事が多いですが、その分達成感が大きい職種だと思います。機械の設定や調整、どう印刷したら見本に合わせられるかを常に考えているので毎日が勉強だと思います。</dd>
								</dl>
							</div>
							<div class="close"><button class="mfp-close"></button></div>
						</section>
						<section id="member-05" class="member-dialog mfp-hide">
							<figure><img src="../assets/images/recruit/member_05_img.jpg" alt=""></figure>
							<div class="wrapper">
									<p class="dept">生産統括部　印刷課</p>
									<p class="name">吉野　俊</p>
									<p class="romaji">Shun Yoshino</p>
									<p class="year">2007年入社</p>
								<dl>
									<dt class="work">あなたの仕事の内容を簡単に教えてください。</dt>
									<dd>本のカバーや帯、本文、表紙などを印刷しています。色調管理装置で印刷物の色調を管理、調整しています。</dd>
									<dt class="heart">仕事のやりがいはなんですか？　どんなことに達成感を感じますか？</dt>
									<dd>ひとつひとつの印刷物に気を遣い、印刷始めから終わりまで同じ色調にする事です。休日に書店へ行き自分自身が携わった本を手に取ると感慨深くなります。</dd>
									<dt class="problem">過去に「失敗したなぁ」という出来事はありますか？　又は、仕事のここが大変！というのはどんなところですか？</dt>
									<dd>コピー機とは違い印刷機械は同じ物を作る事が難しいです。いかに同じ物を作るか、品質を維持できるかを日々精進しています。</dd>
									<dt class="office">あなたの職場はどんな雰囲気ですか？</dt>
									<dd>黙々と仕事をしている個性派（？）揃いの部署です。仕事の合間や昼休みに会話をすると、違った一面がみられ仕事への活力となっていきます。</dd>
									<dt class="message">一言メッセージ</dt>
									<dd>入社して知らない事、分からない事が沢山あると思いますが、その壁を乗り越え、人として社会人としての成長に結びつく事と思います。</dd>
								</dl>
							</div>
							<div class="close"><button class="mfp-close"></button></div>
						</section>
						<section id="member-06" class="member-dialog mfp-hide">
							<figure><img src="../assets/images/recruit/member_06_img.jpg" alt=""></figure>
							<div class="wrapper">
									<p class="dept">総務部　総務経理課</p>
									<p class="name">髙橋 玲美</p>
									<p class="romaji">Remi Takahashi</p>
									<p class="year">2016年入社</p>
								<dl>
									<dt class="work">あなたの仕事の内容を簡単に教えてください。</dt>
									<dd>総務部では、人事・労務関係、経理や設備等の管理を行っています。私は、資料作成を中心に、勤怠管理や事務用品・日用消耗品の発注業務などを担当しています。</dd>
									<dt class="heart">仕事のやりがいはなんですか？　どんなことに達成感を感じますか？</dt>
									<dd>業務内容的にお客様と直接関わる機会は少ないですが、やはり出来上がった印刷物を街中で目にしたときは嬉しくなります。ものづくりの会社なので、会社を通して、社会とのつながりを感じることができます。</dd>
									<dt class="problem">過去に「失敗したなぁ」という出来事はありますか？　又は、仕事のここが大変！というのはどんなところですか？</dt>
									<dd>思い込みや確認不足によって起こしてしまったミスがあります。疑問に思うことはすぐに聞いて解決すること、資料やメールが手元に届いた際は、直ちに中身を確認することが大切だと痛感しています。</dd>
									<dt class="office">あなたの職場はどんな雰囲気ですか？</dt>
									<dd>親切な方が多く、質問のしやすい雰囲気があります。先輩方も丁寧に仕事を指導して下さるので、私も安心して業務に取り組むことができています。</dd>
									<dt class="message">一言メッセージ</dt>
									<dd>就職活動は地道で厳しいものですが、自分にとって一番の選択ができることを願っています。</dd>
								</dl>									
							</div>
							<div class="close"><button class="mfp-close"></button></div>
						</section>
						<section id="member-07" class="member-dialog mfp-hide">
							<figure><img src="../assets/images/recruit/member_07_img.jpg" alt=""></figure>
							<div class="wrapper">
									<p class="dept">営業部　営業管理課</p>
									<p class="name">山上 智美</p>
									<p class="romaji">Tomomi Yamagami</p>
									<p class="year">2007年入社</p>
								<dl>
									<dt class="work">あなたの仕事の内容を簡単に教えてください。</dt>
									<dd>・営業部、各課の事務業務を営業管理課で分担。<br>・各営業業務、顧客毎の関連業務フォロー。</dd>
									<dt class="heart">仕事のやりがいはなんですか？　どんなことに達成感を感じますか？</dt>
									<dd>「大変だ!!」と言われている事案こそ、やる気がでます。マニュアルの無い業務を始める事が多かったので、１から手探りで始まり、担当営業の方と二人で２年間かけて仕組作りが確立した時はとても達成感を感じました。その後、産休へ入らせていただく時にもその仕組みが役に立ち、引継ぎも順調に行うことができました。年々変更箇所は減っていますが誰がみてもわかる仕組み作りを目指して、これからも日々精進できることがやりがいだと思っています☆</dd>
									<dt class="problem">過去に「失敗したなぁ」という出来事はありますか？　又は、仕事のここが大変！というのはどんなところですか？</dt>
									<dd>改善点を上司へ提案した際、会社へ提出する企画書を作ったのは初めてだったので、つたない文章で恐縮しつつも、「良くしたい!!」という気持ちばかりで提案しました。その企画書を受け取った上司から、当時「これをやるのは今ではない。時期が来るのを待ってほしい。」と言われ、落ち込んだことがありました。しかし、その半年後に上司から「やるのは今だ！」とい言われた時はとても嬉しかったです。つたない私の企画書の内容を…今出来る事、実現するための下準備が必要な事、等々半年かけて舵取りしていただいたおかげで、半年間地道に活動していた内容を営業部全体で取り組めることとなりました。良いと思っていても通らない事もたくさんあります。しかし“声を出したら受け止めてくれる上司がいる”ということと“会社という組織の中ではタイミングも大事”なのだと学んだ出来事でした。上司の口癖は「頑張り過ぎず、頑張りましょう！」です。その言葉を忘れずに、これからもより良い職場環境を目指して活動していきたいと思っています。</dd>
									<dt class="office">あなたの職場はどんな雰囲気ですか？</dt>
									<dd>30歳以上歳の離れた若手とベテランが入り混じっていますが、分け隔てなく話しかけ易いです。叱ってくれる上司はいますが、怒る上司はいません。誰かが困っていたら皆さん優しくフォローしてくれたり、時には冗談も言ったりしながら良い意味でとても静かな職場です。</dd>
									<dt class="message">一言メッセージ</dt>
									<dd>就職活動中の皆様、学業との両立大変ですよね・・・お疲れ様です。内定の先にはお仕事と勉強の両立が待っています！ですが、私はフルタイムで働きながら育児もしていますが“お仕事×学び”、“育児×学び”をそれぞれ苦無く両立出来ているのも当社フクインだからこそだと思っています。大変な事もありますが、いろいろな事が両立できる職場で一緒に働ける日が来ることを楽しみにしています。</dd>
								</dl>
							</div>
							<div class="close"><button class="mfp-close"></button></div>
						</section>																								
					  <nav class="more">
						  <ul>
								<li><a href="/service/">フクインのサービスについて</a></li>
								<li><a href="/technique/">フクインの技術について</a></li>
							</ul>
						</nav>
					</section>
          <section id="style">
          	<header>
							<h2>働き方について</h2>
							<p>Work style</p>
						</header>
						<p>フクインでは従業員一人ひとりが、自分の価値観やライフスタイルに従った働き方を選択できるよう<br>
							ワーク・ライフ・バランスの実現に向けた制度を導入しています。</p>
						<ul>
							<li>
								<p>フレックスタイム制度</p>
								<p>業務状況にあわせて、始業時刻・就業時刻を調整できる制度です。公私の両立がしやすく、時間の有効活用が図れます。</p>
							</li>
							<li>
								<p>１ヶ月の変形労働時間制度<br>
									（一部部署）</p>
								<p>１ヵ月内で平均して１週間の労働時間が法定労働時間を超えない範囲で、繁忙期と閑散期の業務量にあわせて、１日や１週間の労働時間を適切に割り当てて働くことのできる制度です。これにより、効率よく業務を進められ、十分に能力を発揮することができます。</p>       
							</li>
							<li>
								<p>産休、育休</p>
								<p>産前産後休暇、育児休業制度を活用してもらい、社員の子育てと仕事の両立を応援しています。</p>
							</li>
							<li>
								<p>CSRについて</p>
								<p>フクインでは、CSR活動も積極的に行っております。CSR活動については以下よりご覧ください。<br>
									<a href="/company/csr/">CSRについて</a></p>
							</li>
						</ul>       
					</section>
          <section id="information">
						<header>
							<h2>フクインを知る</h2>
							<p>Various information of FUKUIN</p>
						</header>
						<p>フクインの社内外の方々にフクインについてアンケートをとりました。<br>
						社内外から見たフクインの特徴をご覧ください。</p>
						<figure><img src="../assets/images/recruit/information_img.jpg" alt=""></figure>
						<section id="information-enquete">
							<h3>アンケートでみるフクイン</h3>
							<ul>
								<li>
									<section id="information-enquete-01">
										<h4>社内から見たフクインの人</h4>
										<ul>
											<li>技術や知識のある人が多く、<br>
												多種多様な製品のノウハウがある</li>
											<li>社歴の長い人が多い</li>
											<li>明るく元気のよい挨拶ができる人が多い</li>
											<li>親切な人が多いから、<br>
												社内コミュニケーションが円滑</li>
											<li>団結力があり、対応が早い</li>
											<li>チャレンジする人が多い</li>
										</ul>
									</section>
								</li>
								<li>
									<section id="information-enquete-02">
										<h4>お客様から見たフクインの人</h4>
										<ul>
											<li>みんな明るくて、挨拶が気持ち良いですね</li>
											<li>笑顔の人が多いね</li>
											<li>フットワークが良く仕事しやすいね</li>
											<li>みなさん電話の対応が良いですね</li>
										</ul>
									</section>
								</li>
								<li>
									<section id="information-enquete-03">
										<h4>フクインで良かったと思うこと</h4>
										<ul>
											<li>文京区にある</li>
											<li>トイレがきれい</li>
											<li>工場が明るくてきれい</li>
											<li>海外出張</li>
											<li>母と入った喫茶店に自分の携わった本が<br>
												置いてあったこと</li>
											<li>フレックスタイム制</li>
										</ul>
									</section>
								</li>
							</ul>
							<figure><img src="../assets/images/recruit/information_enquete_img.png" alt=""></figure>
							<section id="information-enquete-voice">
								<h4>採用担当から一言</h4>
								<p>お客様とも、社員同士でも、<br>
								とにかく笑顔に出会えることが<br>
									多い会社です。</p>
							</section>
						</section>
						<section id="information-data">
							<h3>データでみるフクイン</h3>
							<ul>
								<li><img src="../assets/images/recruit/information_data_01_img.png" alt=""></li>
								<li><img src="../assets/images/recruit/information_data_02_img.png" alt=""></li>
								<li><img src="../assets/images/recruit/information_data_03_img.png" alt=""></li>
							</ul>
							<figure><img src="../assets/images/recruit/information_data_img.png" alt=""></figure>
							<section id="information-data-voice">
								<h4>採用担当から一言</h4>
								<p>因みに、最年少は22才、<br>
								最年長は74才。お母さん社員は3人。<br>
								通勤圏東京都の人は23人。
								</p>
							</section>
						</section>
						<section id="information-fukuinfo">
							<p>フクインについてさらに知りたい方は下記のFUKUINFOをご覧下さい。</p>
							<p class="more"><a href="/fukuinfo/">FUKUINFOを見る</a></p>
						</section>
		        <nav class="recruit">
			        <ul>
     				  	<li><a href="/recruit/fresh/">新卒 募集要項</a></li>
       					<li><a href="/recruit/career/">中途 募集要項</a></li>
       					<li><a href="/recruit/fresh/#fresh">会社説明会・面接会情報</a></li>
							</ul>
						</nav>
					</section>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>