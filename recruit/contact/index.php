<?php error_reporting(E_ALL & ~E_NOTICE); ?>
<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "recruit-contact",
		"css" => array(
			"basic",
			"contact",
			"recruit",
		),
		"js" => array(
		  "https://ajaxzip3.github.io/ajaxzip3.js",
		  "/assets/js/jquery.validate.js",
		  "/assets/js/input_check.js",
		),
		"title" => "採用のお問い合わせ｜採用情報｜株式会社フクイン",
		"description" => "株式会社フクインの採用に関するお問い合わせのページです。",
	));
?>
<script>
	$(document).ready(function() {
<?php if ( $_REQUEST[ 'goback' ] == '1' ) {
	foreach ( $_REQUEST as $key => $value ) { ?>
		checkForm( "<?php echo $key; ?>" );
<?php
		}
	}
?>
	$('#btn_ext').on('click', function() {
		if (!$("#myfrm").validate()) { // Not Valid
			return false;
		} else {
			$("#myfrm").submit();
		}
	});

		$.validator.addMethod(
	        "regex",
	        function(value, element, regexp) {
				value = changeInputData(value);
				$(element).val(value);

	            if (regexp.constructor != RegExp)
	                regexp = new RegExp(regexp);
	            else if (regexp.global)
	                regexp.lastIndex = 0;
	            return this.optional(element) || regexp.test(value);
	        },
	        "Please check your input."
		);

		var validator = $( "#myfrm" ).validate( {
			groups: {
				zip: "zip1 zip2"
			},
			errorPlacement: function( error, element ) {
				if ( element.attr("name") == "zip1" || element.attr("name") == "zip2" ) {
					error.insertAfter( $( '#zip_err' ) );
	　　　　　　}else{
					error.insertAfter( $( '#' + element.attr( 'name' ) + '_err' ) );
	　　　　　　}
			},
			errorElement: "p",
			errorClass: "error",
			rules: {
				mail_cnf: {
					equalTo: '[name="mail"]',
				},
				type:  {required: true}
			},
			messages: {
				sei_namae: {
					required: "姓：お名前は入力必須項目です。",
				},
				mei_namae: {
					required: "名：お名前は入力必須項目です。",
				},
				sei_kana: {
					required: "姓：フリガナは入力必須項目です。",
				},
				mei_kana: {
					required: "名：フリガナは入力必須項目です。",
				},
				company: {
					required: "学校名/会社名は入力必須項目です。",
				},
				mail: {
					required: "メールアドレスは入力必須項目です。",
					email: "正しいメールアドレスを入力してください。",
				},
				mail_cnf: {
					required: "メールアドレス（確認用）は入力必須項目です。",
					email: "正しいメールアドレスを入力してください。",
					equalTo: "メールアドレスが一致しません。",
				},
				zip1: {
					required: "郵便番号は入力必須項目です。",
				},
				zip2: {
					required: "郵便番号は入力必須項目です。",
				},
				pref: {
					required: "県名は入力必須項目です。",
				},
				address1: {
					required: "市区町村は入力必須項目です。",
				},
				tel: {
					required: "電話番号は入力必須項目です。",
					regex: "正しい電話番号を入力してください。",
				},
				body: {
					required: "お問い合わせ内容は入力必須項目です。",
				},
				type: {
					required: "お問い合わせの種類は入力必須項目です。",
				},
				chk: {
					required: "個人情報の取り扱いについてご確認ください。",
				},
			}
		});
	});
</script>
    <div id="visual">
      <div class="site">
        <p>Recruit</p>
        <p>採用情報</p>
      </div>
      <nav class="breadcrumb">
        <ol>
          <li><a href="/">TOP</a></li>
          <li><a href="/recruit/">採用情報</a></li>
          <li><a href="/recruit/contact/">採用に関するお問い合わせ</a></li>
        </ol>
      </nav>
    </div>
    <main>
      <div id="wrapper">
        <div class="site">
          <section id="contact" class="input">
            <header>
              <h2>採用に関するお問い合わせ</h2>
            </header>
            <p>恐れ入りますがお問合わせは、下記のフォームに必要事項をご記入の上送信してください。<br>
              担当者より折り返し連絡させていただきます。</p>
            <p class="required">※印は入力必須項目です。</p>
            <form name="myfrm" id="myfrm" action="conf.php" method="POST">
              <dl>
                <dt>お名前 <span class="required">※</span></dt>
                <dd>
                  <div class="row">
                    <div class="col col-50">
                      <input type="text" name="sei_namae" id="sei_namae" value="<?php echo $_REQUEST['sei_namae']; ?>" placeholder="例）山田" required>
                    </div>
                    <div class="col col-50">
                      <input type="text" name="mei_namae" id="mei_namae" value="<?php echo $_REQUEST['mei_namae']; ?>" placeholder="太郎" required>
                    </div>
                  </div><label id="sei_namae_err"></label><label id="mei_namae_err"></label>
                </dd>
                <dt>フリガナ <span class="required">※</span></dt>
                <dd>
                  <div class="row">
                    <div class="col col-50">
                      <input type="text" name="sei_kana" id="sei_kana" value="<?php echo $_REQUEST['sei_kana']; ?>" placeholder="例）ヤマダ" required onchange="this.value = charHiraganaToKatagana( this.value );this.value = changeInputData( this.value );">
                    </div>
                    <div class="col col-50">
                      <input type="text" name="mei_kana" id="mei_kana" value="<?php echo $_REQUEST['mei_kana']; ?>" placeholder="タロウ" required onchange="this.value = charHiraganaToKatagana( this.value );this.value = changeInputData( this.value );">
                    </div>
                  </div><label id="sei_kana_err"></label><label id="mei_kana_err"></label>
                </dd>
                <dt>学校名又は職業 <span class="required">※</span></dt>
                <dd>
                  <input type="text" name="company" id="company" value="<?php echo $_REQUEST['company']; ?>" placeholder="例）XXX大学/会社員" required onchange="this.value = changeInputData( this.value );">
                 <label id="company_err"></label>
                </dd>
                <dt>メールアドレス <span class="required">※</span></dt>
                <dd>
                  <input type="email" name="mail" id="mail" value="<?php echo $_REQUEST['mail']; ?>" required onchange="this.value = changeInputData( this.value );"><label id="mail_err"></label>
                </dd>
                <dt class="email-conf">メールアドレス <span>（確認用）</span> <span class="required">※</span></dt>
                <dd>
                  <input type="email" name="mail_cnf" id="mail_cnf" value="<?php echo $_REQUEST['mail_cnf']; ?>" required onchange="this.value = changeInputData( this.value );"><label id="mail_cnf_err"></label>
                </dd>
                <dt>ご住所 <span class="required">※</span></dt>
                <dd>
                  <p> 〒
                    <input type="text" name="zip1" id="zip1" value="<?php echo $_REQUEST['zip1']; ?>" placeholder="例）123" class="size-s" required data-validate-numeric data-validate-min-length="3" data-validate-max-length="3" onchange="this.value = changeInputData( this.value );">
                    -
                    <input type="text" name="zip2" id="zip2" value="<?php echo $_REQUEST['zip2']; ?>" placeholder="4567" class="size-s" required data-validate-numeric data-validate-min-length="4" data-validate-max-length="4" onKeyUp="AjaxZip3.zip2addr('zip1','zip2','pref','address1');"><label id="zip_err"></label>
                  </p>
                  <p>
                    <select name="pref" id="pref" required>
                      <option value="">選択して下さい</option>
                      <optgroup label="北海道・東北地方">
<?php $arr = array( '北海道','青森県','岩手県','秋田県','宮城県','山形県','福島県' );
	foreach ( $arr as $value ) { $selected = ''; if ( $value == $_REQUEST['pref'] ) $selected = ' selected'; ?>
	<option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
<?php } ?>
                      </optgroup>
                      <optgroup label="関東地方">
<?php $arr = array( '栃木県','群馬県','茨城県','埼玉県','東京都','千葉県','神奈川県' );
	foreach ( $arr as $value ) { $selected = ''; if ( $value == $_REQUEST['pref'] ) $selected = ' selected'; ?>
	<option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
<?php } ?>
                      </optgroup>
                      <optgroup label="中部地方">
<?php $arr = array( '山梨県','長野県','新潟県','富山県','石川県','福井県','静岡県','岐阜県','愛知県' );
	foreach ( $arr as $value ) { $selected = ''; if ( $value == $_REQUEST['pref'] ) $selected = ' selected'; ?>
	<option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
<?php } ?>
                      </optgroup>
                      <optgroup label="近畿地方">
<?php $arr = array( '三重県','滋賀県','京都府','大阪府','兵庫県','奈良県','和歌山県' );
	foreach ( $arr as $value ) { $selected = ''; if ( $value == $_REQUEST['pref'] ) $selected = ' selected'; ?>
	<option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
<?php } ?>
                      </optgroup>
                      <optgroup label="四国地方">
<?php $arr = array( '徳島県','香川県','愛媛県','高知県' );
	foreach ( $arr as $value ) { $selected = ''; if ( $value == $_REQUEST['pref'] ) $selected = ' selected'; ?>
	<option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
<?php } ?>
                      </optgroup>
                      <optgroup label="中国地方">
<?php $arr = array( '鳥取県','島根県','岡山県','広島県','山口県' );
	foreach ( $arr as $value ) { $selected = ''; if ( $value == $_REQUEST['pref'] ) $selected = ' selected'; ?>
	<option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
<?php } ?>
                      </optgroup>
                      <optgroup label="九州・沖縄地方">
<?php $arr = array( '福岡県','佐賀県','長崎県','大分県','熊本県','宮崎県','鹿児島県','沖縄県' );
	foreach ( $arr as $value ) { $selected = ''; if ( $value == $_REQUEST['pref'] ) $selected = ' selected'; ?>
	<option value="<?php echo $value; ?>"<?php echo $selected; ?>><?php echo $value; ?></option>
<?php } ?>
                      </optgroup>
                    </select><label id="pref_err"></label>
                  </p>
                  <dl class="address">
                    <dt>市区町村</dt>
                    <dd>
                      <input type="text" name="address1" id="address1" value="<?php echo $_REQUEST['address1']; ?>" placeholder="例）文京区音羽 1-23-3" required>
                      <label id="address1_err"></label>
                    </dd>
                    <dt>建物名</dt>
                    <dd>
                      <input type="text" name="address2" id="address2" value="<?php echo $_REQUEST['address2']; ?>" placeholder="例）XXXXビル　●階" onchange="this.value = changeInputData( this.value );">
                    </dd>
                  </dl>
                </dd>
                <dt>お電話番号 <span class="required">※</span></dt>
                <dd>
                  <input type="text" name="tel" id="tel" value="<?php echo $_REQUEST['tel']; ?>" placeholder="例）03-1234-5678" required onchange="this.value = changeInputData( this.value );">
                  <label id="tel_err"></label>
                </dd>
                <dt>お問い合わせの種類 <span class="required">※</span></dt>
                <dd>
                  <label>
                    <input type="radio" name="type" id="type" value="新卒採用についてのお問い合わせ"<?php if ( $_REQUEST['type'] == '新卒採用についてのお問い合わせ' ) echo ' checked'; ?>>
                    <span>新卒採用についてのお問い合わせ</span></label>
                  <br>
                  <label>
                    <input type="radio" name="type" id="type" value="中途採用についてのお問い合わせ"<?php if ( $_REQUEST['type'] == '中途採用についてのお問い合わせ' ) echo ' checked'; ?>>
                    <span>中途採用についてのお問い合わせ</span></label><label id="type_err"></label>
                </dd>
                <dt>お問い合わせ内容 <span class="required">※</span></dt>
                <dd>
                  <textarea name="body" id="body" required><?php echo $_REQUEST['body']; ?></textarea>
                  <label id="body_err"></label>
                </dd>
              </dl>
              <section id="contact-privacy">
                <h3>個人情報について <span class="required">※</span></h3>
                <p>お問い合わせフォームによる当社が取得する個人情報の取り扱いは次の通りです。<br>
                  同意の上、確認画面へお進みください。 </p>
                <div class="scroll">
                  <p> 1. 利用目的<br>
                    当社は、ご本人から書面等で直接お預かりする個人情報については、お預かりする時点で利用目的を明示し、同意をいただいた利用目的の範囲内で個人情報を利用させていただきます。それ以外の方法で取得した個人情報につきましても、下記の利用目的の範囲内でのみ利用させていただきます。 </p>
                  <p> ●お客様<br>
                    当社事業、印刷物の制作・製造・販売における受託業務を執り行うため<br>
                    お問い合わせへの回答、ご請求資料等の送付のため<br>
                    アンケート等のご回答内容を分析し、当社のサービスに反映させるため </p>
                  <p> ●お取引先様<br>
                    業務遂行に伴うご連絡、諸手続きのため </p>
                  <p> 2. ご提供の任意性<br>
                    当社が、お客様ご本人よりご提供いただく情報は、お客様のお名前、会社名、メールアドレス、お電話番号、ご住所といった、お客様に、当社の製品や技術に関して情報を提供する際に必要な個人情報が主な項目になります。なお、お客様の必要に応じた情報の提供等の目的で、これら以外についてお伺いする場合がございますが、この場合は、一部の必須事項を除き、お客様ご自身の選択で情報提供していただくものとしております。但し、ご提供していただけない情報の種類によって、当社からのサービスの一部または全部をご提供できない場合があります。 </p>
                  <p> 3. 自動取得する情報<br>
                    (1)当社は、お客様よりお電話をいただいた場合、お客様のお申し出を聞き漏らさないように、通話内容を録音させていただくことがあります。<br>
                    (2)当社Ｗｅｂサイトでは、当社自身のＷｅｂサイトの評価のため、アクセスログを記録しております。なお、クッキー（Cookie）は使用しておりません。 上記、いずれの自動取得情報も、当社の情報セキュリティ関連規定に従い適切に管理しております。 </p>
                  <p> 4. 利用及び第三者への提供<br>
                    当社は、以下の場合を除き、個人情報を利用目的の達成に必要な範囲を超えて利用をおこなったり、第三者に提供をいたしません。<br>
                    (1)お客様の同意がある場合(第三者に提供する場合には、原則として、機密保持、再提供の禁止、お客様のお申し出により利用を停止することを契約の条件といたします）<br>
                    (2)法令等に基づく場合<br>
                    (3)ご本人または公衆の生命、身体及び財産の保護のために必要がある場合であって、ご本人の同意を得ることが困難であるとき </p>
                  <p> 5. Webサイトでの個人情報の取扱い<br>
                    当社では、個人情報等の保護の必要なデータを安全にやりとりするために、SSL（Secure Socket Layer）を利用した暗号化通信を使用しています。当サイトのWebサーバと、お客様がお使いのブラウザ間の情報はSSL技術により保護されています。<br>
                    ※ 社内LAN接続からファイアーウォールを経由している場合等で、その設定によってSSLによる通信をご利用できない場合があります。その際はネットワーク管理者様にご確認ください。また、本サイトからリンクされた、他社が管理するWebサイトの個人情報の安全管理、内容やサービスに関して、当社では一切の責任を負うことはできません。他社で管理しているサイトの安全管理につきましては、当該リンク先の個人情報保護方針等をご確認ください。 </p>
                </div>
                <p> 尚、個人情報に関するお問い合わせなどがある場合は「<a href="/company/privacy/">株式会社フクインの個人情報に関する基本方針</a>」のご確認をお願いします。 <br>
                  履歴書等をご送付していただく際は、「<a href="/assets/pdf/recruit_consent_form.pdf" target="_blank">採用応募者個人情報の取扱い及び利用に関する同意書</a>」をプリントアウトしていただき、ご署名、ご捺印の上ご同封下さい。 </p>
                <div class="agreement">
                  <label>
                    <input type="checkbox" name="chk" id="chk" value="1" required<?php if ( $_REQUEST['chk'] == '1' ) echo ' checked'; ?>>
                    個人情報の取り扱いについて同意する</label><label id="chk_err"></label>
                </div>
              </section>
              <div class="action">
                <button type="submit" name="btn_ext" id="btn_ext" class="btn">確認画面へ進む</button>
              </div>
			<input type="hidden" name="ext" value="1">
            </form>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>