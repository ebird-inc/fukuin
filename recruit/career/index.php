<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "recruit",
		"css" => array(
			"basic",
			"recruit",
		),
		"title" => "採用情報｜株式会社フクイン",
		"description" => "株式会社フクインの採用情報のページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Recruit</p>
        <p>採用情報</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/recruit/">採用情報</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>FUKUINの採用</h1>
            <p>We will inform you of recruitment information.</p>
          </header>
          <section id="career">
            <header>
              <h2>中途採用</h2>
            </header>
            <p>現在募集しておりません</p>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>