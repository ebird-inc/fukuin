<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "company-access",
		"css" => array(
			"basic",
			"company",
		),
		"title" => "アクセス｜会社案内｜株式会社フクイン",
		"description" => "株式会社フクインのアクセスのページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Corporate Information</p>
        <p>会社案内</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/company/">会社案内</a></li>
        <li><a href="./">アクセス</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>アクセス</h1>
            <p>Access</p>
          </header>
          <dl class="access">
            <dt>音羽工場第1ビル</dt>
            <dd>
              <figure><img src="../../assets/images/company/access/access_01.png" alt=""></figure>
              <p> 〒112-0013 東京都文京区音羽1-23-3<br>
                工場総合受付 03-3946-2289(代)<br>
                <a href="https://www.google.co.jp/maps/place/〒112-0013+東京都文京区音羽１丁目２３−３/@35.7132833,139.7275628,17z/" target="_blank">グーグル・マップで見る</a></p>
            </dd>
            <dt>音羽工場志賀ビル</dt>
            <dd>
              <figure><img src="../../assets/images/company/access/access_02.png" alt=""></figure>
              <p>〒112-0013 東京都文京区音羽1-23-22 志賀ビル1F<br>
                <a href="https://www.google.co.jp/maps/place/〒112-0013+東京都文京区音羽１丁目２３−２２/@35.7130472,139.7276156,17z/" target="_blank">グーグル・マップで見る</a></p>
            </dd>
            <dt>音羽工場第3ビル</dt>
            <dd>
              <figure><img src="../../assets/images/company/access/access_03.png" alt=""></figure>
              <p>〒112-0013 東京都文京区音羽1-24-7<br>
                総務 03-3946-2281(代)<br>
                <a href="https://www.google.co.jp/maps/place/〒112-0013+東京都文京区音羽１丁目２４−７/@35.7128833,139.7276823,17z/" target="_blank">グーグル・マップで見る</a></p>
            </dd>
          </dl>
          <figure class="map"><img src="../../assets/images/company/access/map_img.png" alt=""></figure>
          <p>▶地下鉄有楽町線「江戸川橋」駅1a出口より徒歩8分。<br>
            ▶地下鉄有楽町線「護国寺」駅6番出口より徒歩10分。<br>
            ▶お車の場合は音羽通りより→に従ってご来社下さい。<br>
            なお、お車でお越しの際はお近くの有料駐車場をご利用下さい。<br>
            ※エントランスは高速側になります</p>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>