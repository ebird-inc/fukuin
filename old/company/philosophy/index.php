<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "company-philosophy",
		"css" => array(
			"basic",
			"company",
		),
		"title" => "理念｜会社案内｜株式会社フクイン",
		"description" => "株式会社フクインの理念のページです。フクインは「時代」を伝えることで、社会に貢献し、フクインは表現を「創造」することで会社を継続、利益創造のパートナーとして、信頼できる情報を過不足なく伝えることを目標としています。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Corporate Information</p>
        <p>会社案内</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/company/">会社案内</a></li>
        <li><a href="./">理念</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>理念</h1>
            <p>Philosophy</p>
          </header>
          <dl>
            <dt>時代を伝えます</dt>
            <dd>
              <p>印刷は情報産業の一翼を担っています。<br>
                情報から新しい時代や文化が生まれます。<br>
                フクインは「時代」を伝えることで、社会に貢献します。 </p>
            </dd>
            <dt>表現を創造します</dt>
            <dd>
              <p>情報は対象・目的・用途により、個々に適切な伝達手法が存在します。<br>
                本当に伝えたいことを最適に表現するために。<br>
                フクインは表現を「創造」することで、会社を継続させていきます。 </p>
            </dd>
            <dt>夢・未来へ</dt>
            <dd>
              <p>発信・伝達・継承する情報は、社会に果てしなく広がっていきます。<br>
                お客様の利益創造のパートナーとして、<br>
                信頼できる情報を過不足なく伝えることがフクインの夢。<br>
                そこには幸せな未来が待っています。 </p>
              <p>フクインは「夢・未来」に向かって歩み続けます。 </p>
            </dd>
          </dl>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>