<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "service",
		"css" => array(
			"basic",
			"service",
		),
		"title" => "映画館プロモーション｜サービス｜株式会社フクイン",
		"description" => "株式会社フクインのサービスのページです。フクインのフルサービスは、デザインから印刷まで一貫したサービスを提供、密接なコミュニケーションで「納期」「コスト」だけでなく、お客様の「手間」を削減します。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Service</p>
        <p>サービス</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/service/">サービス</a></li>
        <li><a href="/service/theater">映画館プロモーション</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>FUKUINのサービス</h1>
            <p>We will try our best to provide better services.</p>
          </header>
          <section>
            <header>
              <h2>映画館プロモーション</h2>
            </header>
            <section>
              <h3 class="h4">映画館サンプリングとは</h3>
              <p>映画館という非常に限定された空間を利用して広告や宣伝を行う、映画館販促です。<br>
              ＜チラシを作成＞ → ＜映画館で配布＞という流れがワンストップで行えます。</p>

              <p>例えば…<br>
              チケット引き渡し時に、チラシも一緒に渡す。これならば来場者は必ずチラシを手にすることになる、画期的なプロモーションです。</p>
              <figure><img src="../../assets/images/service/theater/theater_01.png" alt=""><img src="../../assets/images/service/theater/theater_02.png" alt=""></figure>
            </section>
            <section>
              <h3 class="h4">point1　ニーズに合わせたターゲティングが出来ます</h3>
              <p>映画の内容によって、学生、OL、ファミリー、シニアなど来場者が変わります。<br>
                来場者予測に基づき、御社のニーズに合わせた展開が可能です。 </p>
              <figure><img src="../../assets/images/service/theater/theater_03.png" alt=""><img src="../../assets/images/service/theater/theater_04.png" alt=""></figure>
            </section>
            <section>
              <h3 class="h4">point2　シネコン※1館からでもOK</h3>
              <p>「映画館プロモーション」は、地域を問わずシネコン1 館から行えますので、地域の特性を生かしたプロモーションが出来ます。<br>
                地元や特定地域での知名度、認知度アップが見込める宣伝効果があります。</p>
              <p>＊シネコン … シネマコンプレックスの略。同一の施設に複数のスクリーンがある映画館を指す。</p>
            </section>
            <section>
              <h3 class="h4">point3　高いリーセンシー効果※を生み出します</h3>
              <p>映画の内容に関係するものや、映画の舞台になった地域に関わるものなどをプロモーションすることで、映画との相乗効果により高いリーセンシー効果を生み出します。</p>
              <p>例えば…<br>
                子供向け映画に合わせて、小学生向け商品のチラシを配布するなどで、購買意欲がより高まります。</p>
              <P>＊リーセンシー効果 … 直前に接触した広告が、購買行動に影響を与える効果。（日本民間放送連盟の定義）</p>
            </section>
            <section>
              <h3 class="h4">他に映画館ではどんなプロモーションができるの？</h3>
              <p>チラシ配布に限らず、映画館を利用した様々な宣伝が行えるのが、「映画館プロモーション」です。</p>
              <p><strong>チケット引き渡し時のプロモーション</strong><br>
                お客様がチケットを購入される際に、チケットと合わせてサンプルなども配布することが出来ます。地域や性別などでセグメントしてのチラシ配布やサンプル配布なども出来ます。 </p>
              <p><strong>ロビーでのプロモーション</strong><br>
                チケット購入時にターゲットとなるお客様へ案内チラシを配布して告知を行います。<br>
                映画上映までの待ち時間を利用し、スタッフによる商品の細かい説明をお客様にお聞かせすることが出来ます。<br>
                また、配布以外にも映画館内でのチラシの設置、ポスターの掲示、車両展示（一部シネコンのみ）なども行うことが出来ます。 </p>
              <figure><img src="../../assets/images/service/theater/theater_05.png" alt=""><img src="../../assets/images/service/theater/theater_06.png" alt=""></figure>
            </section>
            <section>
              <p><strong>映画を利用したプロモーション</strong><br>
                映画本編上映前にコマーシャルを上映することが出来ます。<br>
                上映中は会話や携帯電話使用などが制限されるため、必然的に上映されているものを見る事になり、観客に対しての視聴率は100％に近くなります。 </P>
              <figure><img src="../../assets/images/service/theater/theater_07.png" alt=""><img src="../../assets/images/service/theater/theater_08.png" alt=""></figure>
            </section>
            <section>
              <p> 映画館別などで行えるので、ご予算に合わせたプロモーションが出来ます。<br>
                例えば… </p>
              <p><strong>東京エリアでチラシ配布プロモーションを実施した場合</strong></p>
              <figure><img src="../../assets/images/service/theater/theater_09.png" alt=""></figure>
              <p><a href="/assets/pdf/tokyoplan.pdf">▶ モデルプランの詳細はこちら</a></p>
              <p><strong>東関東エリアでチラシ配布プロモーションを実施した場合</strong><br>
              <figure><img src="../../assets/images/service/theater/theater_10.png" alt=""></figure>
              <p><a href="/assets/pdf/kantoplan.pdf">▶ モデルプランの詳細はこちら</a></p>
            
              <p>また、映画館スタッフによる退場時でのサンプリングやチラシ配布などの組み合わせが出来ます。<br>
              その他、映画館を利用した様々なプロモーションが、デザイン、制作、印刷からワンストップで出来ます。</p>

              <p>※映画館プロモーションの手配については、（株）シネブリッジが行ないます。<br>
              詳しくはお問い合わせにてご相談ください。</p>
            </section>
            </header>


            <section>
              <h3>フルサービス </h3>
              <p class="more"><a href="/service/" class="btn">詳細はこちら</a></p>

            </section>
            <section>
              <h3>Webプロモーション </h3>
              <p class="more"><a href="/service/web/" class="btn">詳細はこちら</a></p>

            </section>



          </section>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>