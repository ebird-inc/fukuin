<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "service",
		"css" => array(
			"basic",
			"service",
		),
		"title" => "フルサービス｜サービス｜株式会社フクイン",
		"description" => "株式会社フクインのサービスのページです。フクインのフルサービスは、デザインから印刷まで一貫したサービスを提供、密接なコミュニケーションで「納期」「コスト」だけでなく、お客様の「手間」を削減します。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Service</p>
        <p>サービス</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/service/">サービス</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>FUKUINのサービス</h1>
            <p>We will try our best to provide better services.</p>
          </header>
          <section id="fullservice">
            <header>
              <h2>フルサービス</h2>
              <p>デザインから印刷まで一貫したサービスをご提供いたします。<br>
                密接なコミュニケーションをとりながら、「納期」や「コスト」だけでなく<br>
                お客様の「手間」も削減していきます。</p>
            </header>
            <section>
              <h3>デザインから納品までを一括管理</h3>
              <p>制作会社や製本会社といったグループ企業との連携により、<br>
                デザインから納品までの一貫サービスが可能となりました。<br>
                モノづくりで発生する様々な工程の作業を一手にお引き受けすることにより、<br>
                お客様の「手間」や「コスト」を抑えるお手伝いをいたします。生産工程のみならず、<br>
                製品完成後の在庫管理に関してもお任せください。 </p>
              <figure><img src="../assets/images/service/fullservice_01_img.jpg" alt=""></figure>
            </section>
            <section>
              <h3>各工程の高いディレクション能力</h3>
              <p>担当営業が、お客様の「つくりたいモノ」「スケジュール」「予算」などを管理しながら、<br>
                一貫してモノづくりをサポートしていきます。各部署と連携をとりながら、<br>
                お客様のご要望に最適なメディアでのモノづくりをご提案しております。 </p>
              <figure><img src="../assets/images/service/fullservice_02_img.jpg" alt=""></figure>
            </section>
            <section>
              <h3>パートナー企業との連携</h3>
              <p>お客様の多彩なニーズに応えるため、150社以上の様々な強みをもった企業と連携しております。<br>
                「目を惹くモノをつくりたい」「少し変わったモノをつくりたい」といったお客様の様々なご要望を、フクインと
                パートナー企業がカタチにしていきます。<br>
                また、情報伝達企業として、紙のメディア以外にも、パートナー企業との連携による様々なプロモーションを
                ご提案しております。 </p>
              <figure><img src="../assets/images/service/fullservice_03_img.jpg" alt=""></figure>
            </section>
          </section>
          <section id="promotion">
            <header>
              <h2>プロモーション</h2>
              <p>フクインは様々なパートナー企業との連携により効果の高い販売促進プロモーションをご提案しています。
                その中でフクインがお勧めする「映画館プロモーション」「印刷×Webプロモーション」をご紹介します。
              </p>
            </header>
            <section>
              <figure><img src="../assets/images/service/promotion_01_img.jpg" alt=""></figure>
              <h3>映画館プロモーション </h3>
              <p>映画館プロモーションは大型ショッピングセンターに入っているシネコンを利用して、実際の店舗に集客・送
                客できることが特徴です。売り込みたい商品やサービスと、上映される映画から見込める客層に対してサン
                プリングが可能。併設された店舗に誘導まで行えるプロモーションが実現可能です。 </p>
             
              <p class="more"><a href="/service/theater/" class="btn">詳細はこちら</a></p>

            </section>
            <section>
              <h3>Webプロモーション </h3>
              <p>デバイスの進歩により、Webプロモーションは日々変化していきます。このような変化の時代だからこそ、長
                い視点で正解を共に導けるパートナー企業を選ぶことが最も重要となってきます。フクインは、長年お客様
                と共に歩んできた印刷業で培ったヒヤリング・マネジメント能力でサポート。入念な対話により、Webと印刷
                物を利用して実現できることを総合的にご提案いたします。 </p>

              <p class="more"><a href="/service/web/" class="btn">詳細はこちら</a></p>

            </section>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>