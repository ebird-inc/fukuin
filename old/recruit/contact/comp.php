<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "contact",
		"css" => array(
			"basic",
			"contact",
		),
		"title" => "採用のお問い合わせ｜採用情報｜株式会社フクイン",
		"description" => "株式会社フクインの採用に関するお問い合わせのページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Recruit</p>
        <p>採用情報</p>
      </div>
      <nav class="breadcrumb">
        <ol>
          <li><a href="/">TOP</a></li>
          <li><a href="/recruit/">採用情報</a></li>
          <li><a href="/recruit/contact/">採用に関するお問い合わせ</a></li>
        </ol>
      </nav>
    </div>
    <main>
      <div id="wrapper">
        <div class="site">
          <section id="contact" class="comp">
            <header>
              <h2>採用に関するお問い合わせ</h2>
            </header>
            <p class="message">送信が完了しました。</p>
            <p>お問い合わせありがとうございます。<br>
              後日、担当者よりお返事差し上げます。<br>
              尚、お問い合せの内容によっては、返信に時間がかかる場合や、<br>
              お答えできない場合があることをご了承ください。</p>
            <div class="action"> <a href="/" class="btn home">トップに戻る</a> </div>
            </form>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>