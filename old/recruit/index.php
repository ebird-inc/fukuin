<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "recruit",
		"css" => array(
			"basic",
			"recruit",
		),
		"title" => "採用情報｜株式会社フクイン",
		"description" => "株式会社フクインの採用情報のページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Recruit</p>
        <p>採用情報</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/recruit/">採用情報</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>FUKUINの採用</h1>
            <p>We will inform you of recruitment information.</p>
          </header>
          <nav class="jump">
            <ul>
              <li><a href="#career"><span>中途採用</span></a></li>
            </ul>
          </nav>
          <section id="fresh">
            <header>
              <h2>新卒採用</h2>
            </header>
            <section>
              <h3 class="h4">募集要項</h3>
              <dl>
                <dt>募集職種</dt>
                <dd>総合職(企画事務・事務) 最大2名 </dd>
                <dt>仕事の内容</dt>
                <dd>
                  <p><strong>企画事務</strong><br>
                    社内外の文書や分析資料の作成を中心に、取締役や部長のスケジュール管理や電話の取次ぎ等サポートを行います。また、会社の広報業務である自社ホームページの運営管理等のサポートも担当して頂く予定です。 </p>
                  <p><strong>事務</strong><br>
                    伝票入力や資料作成を行います。<br>
                    その他、電話や来客の対応、備品の在庫・発注管理、社内への広報等、様々な庶務も担当していただく予定です。</p>
                </dd>
                <dt>応募資格</dt>
                <dd>大学院、大学、短大、専修学校 卒業者対象<br>
                  2018年3月卒業・修了見込<br>
                  または既卒5年以内の方</dd>
                <dt>求める人物像</dt>
                <dd>・挨拶がきちんとできる方<br>
                  ・学ぶ姿勢を持ってコツコツと、投げ出さずに仕事に取り組める方<br>
                  ・印刷物やWEBに興味がある方</dd>
                <dt>選考過程</dt>
                <dd>応募書類(下記参照)を直接ご送付下さい。<br>
                  ↓<br>
                  書類選考<br>
                  ↓<br>
                  一次面接・筆記試験(一般常識・作文)<br>
                  ↓<br>
                  二次面接<br>
                  ↓<br>
                  最終面接(二次面接が最終となることもあります)<br>
                  ↓<br>
                  内定</dd>
                <dt>応募書類</dt>
                <dd>履歴書、成績証明書、卒業見込書(卒業証明書)、<a href="/assets/pdf/recruit_consent_form.pdf" target="_blank">採用応募者個人情報の取扱い及び利用に関
                  する同意書</a>(各自プリントアウトの上、ご署名・ご捺印いただき、同封をお願い致します)</dd>
                <dt>書類提出先</dt>
                <dd>〒112-0013 東京都文京区音羽1-24-7<br>
                  株式会社フクイン 総務部 採用担当者宛</dd>
              </dl>
            </section>
            <section>
              <h3 class="h4">新卒採用データ</h3>
              <dl>
                <dt>雇用形態</dt>
                <dd>正社員</dd>
                <dt>初任給</dt>
                <dd>大学・専門学校卒業/基本給18万5千円<br>
                  高等学校卒業/基本給16万5千円(現在高卒の募集は行っておりません)<br>
                  ※その他、法定・就業規則の条件に基づく支給手当(住宅手当、家族手当、資格手当等)あり<br>
                  ※賞与は会社業績と個人の成績に基づき支給額を決定<br>
                  ※昇給は個人の実績、勤務態度等を考慮の上決定 </dd>
                <dt>勤務地</dt>
                <dd>東京都文京区音羽(本社/営業所/工場)</dd>
                <dt>勤務時間</dt>
                <dd>通常勤務/9:00~18:00<br>
                  ※フレックスタイム制有り</dd>
                <dt>休日休暇</dt>
                <dd>休日  土曜、日曜、祝日<br>
                  年間休日 113日(会社カレンダーによる)<br>
                  休暇  夏期休暇、年末年始、慶弔など </dd>
                <dt>加入保険</dt>
                <dd>健康保険、厚生年金、雇用保険、労災保険</dd>
                <dt>福利厚生</dt>
                <dd>保養所(箱根)、介護・育児休暇制度、再雇用制度、退職金制度</dd>
                <dt>その他</dt>
                <dd>off-JTによる研修制度あり、クラブ活動(ゴルフ部、文化鑑賞部など) </dd>
              </dl>
            </section>
            <nav class="action">
              <p><a href="/recruit/contact/" class="btn">採用に関するお問い合わせ</a></p>
            </nav>
          </section>
          <section id="career">
            <header>
              <h2>中途採用</h2>
            </header>
            <p>現在募集しておりません</p>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>