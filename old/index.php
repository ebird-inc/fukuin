<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "home",
		"css" => array(
			"home",
		),
		"title" => "総合印刷会社｜株式会社フクイン",
		"description" => "株式会社フクインは東京都文京区で1948年創業の印刷会社。DDCPによる簡易色校正で低コスト・高品質・短納期を実現します。フクインは組版から製本まで一貫生産でお客様の印刷をサポートします。",
	));
?>
    <section id="visual">
      <ul>
        <li>
          <figure>
            <div class="copy">
              <p><img src="assets/images/common/70thlogo_black.svg"></p>
              <p>株式会社フクインは創業から70周年を迎えます。<br>
                これからも「人」を磨き「印刷」と共に歩みます。 </p>
            </div>
            <div class="visual"></div>
          </figure>
        </li>
        <li>
          <figure>
            <div class="copy">
              <p><img src="assets/images/common/70thlogo_black.svg"></p>
              <p>株式会社フクインは創業から70周年を迎えます。<br>
                これからも「人」を磨き「印刷」と共に歩みます。</p>
            </div>
            <div class="visual"></div>
          </figure>
        </li>
        <li>
          <figure>
            <div class="copy">
              <p><img src="assets/images/common/70thlogo_white.svg"></p>
              <p>株式会社フクインは創業から70周年を迎えます。<br>
                これからも「人」を磨き「印刷」と共に歩みます。</p>
            </div>
            <div class="visual"></div>
          </figure>
        </li>
      </ul>
    </section>
    <section id="message">
      <div class="site">
        <header>
          <h2><img src="assets/images/home/message_title.svg" alt="Message"></h2>
          <p class="copy"> We support you in your business with various services. </p>
          <p class="title">メッセージ </p>
          <p class="lead">株式会社フクインは、印刷会社として始まり、<span class="for-pc"><br>
            </span> デジタルデバイスへの対応を含めた情報伝達企業として未来へ向かっています。<br>
            お客様のニーズを汲み取り、70年の歴史で積み重ねた<span class="for-pc"><br>
            </span> 研ぎ澄まされた技術力とサービス力で<span class="for-pc"><br>
            </span> 高品質を実現し、お客様のビジネスをサポートします。</p>
        </header>
      </div>
    </section>
    <section id="knowhow" class="scrollin">
      <div class="site">
        <header>
          <h2><img src="assets/images/home/knowhow_title.svg" alt="Know-How"></h2>
          <p class="title"> FUKUINの技術 </p>
          <p class="lead">長い歴史の中で培われたフクインの技術。<br>
            お客様の求める品質を実現するために、様々なノウハウを駆使し高いクオリティで製品をお届けしています。</p>
        </header>
        <ul>
          <li> <a href="/technique/">
            <p class="title">組版・制作</p>
            <p>Digitization</p>
            <p>国内でもいち早くスタートしたDTP技術と、写真加工のノウハウ、データベース運用の実績はカタログ制作から書籍組版まで。</p>
            </a> </li>
          <li> <a href="/technique/proof/">
            <p class="title">色校正</p>
            <p>Color Proof</p>
            <p>完成のイメージをかたちにする手段として、最適な色校正を提供しています。
              本機校正、本紙校正、DDCPなどをご用意。</p>
            </a></li>
          <li> <a href="/technique/printing/">
            <p class="title">印刷</p>
            <p>Printing</p>
            <p>CTPの高い技術をベースに、インキの工夫やアミ点技術などを駆使し、より品質の高い印刷を実現しています。</p>
            </a> </li>
          <li> <a href="/technique/process/">
            <p class="title">製本・加工</p>
            <p>Binding / Finishing</p>
            <p>製品完成の最終段階、加工や製本工程技術とプロセス管理で、高品質な出来上がりを納期通りに完成させます。</p>
            </a> </li>
          <li> <a href="/technique/trobleshooting/">
            <p class="title">トラブル解決</p>
            <p>Troubleshooting</p>
            <p>DTP制作から印刷、製本工程まで、フクインには長年の実績で蓄積されたトラブル解決の方法があります。</p>
            </a></li>
          <li> <a href="/technique/case/">
            <p class="title">製品事例</p>
            <p>Product Introduction</p>
            <p>お客様のご要望を製品として完成させるために様々な提案をしています。</p>
            </a> </li>
        </ul>
      </div>
    </section>
    <section id="service">
      <div class="site">
        <header>
          <h2><img src="assets/images/home/service_title.svg" alt="Our services"></h2>
          <p class="copy">We will try our best to provide better services to our customers.</p>
          <p class="title">サービスについて</p>
          <p class="lead"> お客様と市場の間にある課題を、デザイン、制作、印刷を通じて解決いたします。<br>
            お客様とのコミュニケーションの中から提案を行い、<br>
            必要に応じたサービスをご提供いたします。 </p>
        </header>
      </div>
      <ul>
        <li class="scrollin"> <a href="/service/#fullservice">
          <div class="container">
            <figure><img src="assets/images/home/service_01_img.jpg" alt=""></figure>
          </div>
          <div class="container">
            <p class="title">フルサービス</p>
            <p>Total support </p>
            <p>デザインから印刷まで一貫したサービスをご提供
              いたします。密接なコミュニケーションをとりながら、「納期」や「コスト」だけでなくお客様の「手間」も削減していきます。 </p>
          </div>
          </a> </li>
        <li class="scrollin"> <a href="/service/#promotion">
          <div class="container">
            <figure><img src="assets/images/home/service_02_img.jpg" alt=""></figure>
          </div>
          <div class="container">
            <p class="title">プロモーション</p>
            <p>Promotion </p>
            <p>プロモーションツールはWebや販促ツール、紙媒体など多岐にわたりますが、フクインは様々なパートナー企業との連携により幅広い対応が可能です。映画館を利用したリアルプロモーションや、紙媒体とWeb制作を組み合わせることで、費用対効果の高いプロモーションをご提案いたします。 </p>
          </div>
          </a> </li>
      </ul>
    </section>
<?php /*    
    <section id="works">
      <div class="site">
        <header>
          <h2><img src="assets/images/home/works_title.svg" alt="Works"></h2>
          <p class="copy">Show case of Fukuin. </p>
          <p class="title">実績</p>
          <p class="lead">株式会社フクインの実績の一部をご紹介します。 </p>
        </header>
      </div>
      <ul>
        <li><a href="">
          <figure><img src="assets/images/home/works_img.jpg" atl=""></figure>
          <div class="container">
            <p class="date">2017.01.20</p>
            <p class="category">パンフレット</p>
            <p class="title"> 株式会社ダミースカイブルーライト様 
              会社案内ツール </p>
            <p> WE ARE CHALLENGE </p>
          </div>
          </a></li>
        <li><a href="">
          <figure><img src="assets/images/home/works_img.jpg" atl=""></figure>
          <div class="container">
            <p class="date">2017.01.20</p>
            <p class="category">パンフレット</p>
            <p class="title"> 株式会社ダミースカイブルーライト様 
              会社案内ツール </p>
            <p> WE ARE CHALLENGE </p>
          </div>
          </a></li>
        <li><a href="">
          <figure><img src="assets/images/home/works_img.jpg" atl=""></figure>
          <div class="container">
            <p class="date">2017.01.20</p>
            <p class="category">パンフレット</p>
            <p class="title"> 株式会社ダミースカイブルーライト様 
              会社案内ツール </p>
            <p> WE ARE CHALLENGE </p>
          </div>
          </a></li>
      </ul>
      <p class="more"><a href="">MORE </a></p>
    </section>
    <section id="fukuinfo">
      <div class="site">
        <header>
          <h2><img src="assets/images/home/fukuinfo_title.svg" alt="FUKUINFO"></h2>
          <p class="copy"> Information of Fukuin. </p>
          <p class="title">フクインフォ</p>
          <p class="lead"> フクインの人や印刷に関わる基礎話をお届けします。</p>
        </header>
      </div>
      <ul>
        <li> <a href="">
          <figure><img src="assets/images/home/fukuinfo_01_img.jpg" alt=""></figure>
          <div class="container">
            <p class="date">2017.02.25</p>
            <p class="category"> フクインの人</p>
            <p class="series"> 田島さん×吉川さん 対談②</p>
            <p class="title"> 印刷について</p>
            <p class="excerpt"> 豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
            <p class="more">続きを読む</p>
          </div>
          </a> </li>
        <li> <a href="">
          <figure><img src="assets/images/home/fukuinfo_02_img.jpg" alt=""></figure>
          <div class="container">
            <p class="date">2017.02.25</p>
            <p class="category"> フクインの人</p>
            <p class="series"> 田島さん×吉川さん 対談②</p>
            <p class="title"> 印刷について</p>
            <p class="excerpt"> 豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
            <p class="more">続きを読む</p>
          </div>
          </a> </li>
        <li> <a href="">
          <figure><img src="assets/images/home/fukuinfo_03_img.jpg" alt=""></figure>
          <div class="container">
            <p class="date">2017.03.06</p>
            <p class="category"> 印刷の〇〇話 </p>
            <p class="series">第ニ回</p>
            <p class="title"> モニターと印刷物の色の違い</p>
            <p class="excerpt"> 豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
            <p class="more">続きを読む</p>
          </div>
          </a> </li>
        <li> <a href="">
          <figure><img src="assets/images/home/fukuinfo_04_img.jpg" alt=""></figure>
          <div class="container">
            <p class="date">2017.03.01</p>
            <p class="category"> 印刷の〇〇話 </p>
            <p class="series">第一回</p>
            <p class="title"> 印刷の種類としくみ</p>
            <p class="excerpt"> 豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた... </p>
            <p class="more">続きを読む</p>
          </div>
          </a> </li>
      </ul>
      <p class="more"><a href="">MORE </a></p>
    </section>
*/
?>		
    <div id="secondary">
      <section id="news">
        <div class="inner">
          <h2><img src="assets/images/home/news_title.svg" alt="News"></h2>
          <ul>
            <li><span>
              <p class="date"> 2017.06.30 </p>
              <p class="title">ホームページをリニューアルいたしました。 </p>
              </span> </li>
            <li><span>
              <p class="date"> 2017.02.09 </p>
              <p class="title">2月28日ハローワーク主催の2017年3月卒対象面接会を実施いたします！ </p>
              </span> </li>
            <li><span>
              <p class="date"> 2016.09.05 </p>
              <p class="title">新卒採用選考応募書類受付開始いたしました。 </p>
              </span> </li>
            <li><span>
              <p class="date"> 2016.08.29 </p>
              <p class="title">営業部が銀座営業所から音羽本社に移転し、統合いたします。 </p>
              </span> </li>
          </ul>
        </div>
      </section>
      <section id="group">
        <header>
          <h2><img src="assets/images/home/group_title.svg" alt="Group company"></h2>
          <p class="title">グループ企業</p>
        </header>
        <ul>
          <li><a href="https://www.fukuin-graphic.com/" target="_blank"><img src="assets/images/home/group_01_logo.png" alt="フクイングラフィック"></a></li>
          <li><a href="http://www.meirin.biz/" target="_blank"><img src="assets/images/home/group_02_logo.png" alt="メイリン"></a></li>
        </ul>
      </section>
    </div>
<?php
	get_footer();
?>