<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "works",
		"css" => array(
			"basic",
			"works",
		),
		"title" => "実績｜株式会社フクイン",
		"description" => "",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Works</p>
        <p>実 績</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/works/">実績</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>ダミーテキストダミーテキスト</h1>
            <p>ダミーテキストダミーテキストダミーテキストダミーテキスト</p>
          </header>
          <section>
            <header>
              <h2>ダミーテキスト</h2>
              <p>ダミーテキスト</p>
            </header>
            <p>ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト ダミーテキストダミーテキストダミーテキストダミーテキストダミーテキスト</p>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>