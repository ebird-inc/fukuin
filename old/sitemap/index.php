<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header(array(
		"body_class" => "sitemap",
		"css" => array(
			"basic",
			"sitemap",
		),
		"title" => "サイトマップ｜株式会社フクイン",
		"description" => "株式会社フクインのサイトマップのページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <h1>Sitemap</h1>
        <p>サイトマップ</p>
      </div>
      <nav class="breadcrumb">
        <ol>
          <li><a href="/">TOP</a></li>
          <li><a href="/sitemap/">サイトマップ</a></li>
        </ol>
      </nav>
    </div>
    <main>
      <div id="wrapper">
        <div class="site">
          <div class="row">
            <div class="col">
              <ul>
                <li class="home"><a href="/"><span>TOP</span></a>
                <li class="company"><a href="/company/"><span>会社案内</span></a>
                  <ul>
                    <li><a href="/company/">社長挨拶</a></li>
                    <li class="philosophy"><a href="/company/philosophy/">理念</a></li>
                    <li class="profile"><a href="/company/profile/">企業概要</a></li>
                    <li class="development"><a href="/company/development/">沿革</a></li>
                    <li class="history"><a href="/company/history/">歴史</a></li>
                    <li class="equipment"><a href="/company/equipment/">設備一覧</a></li>
                    <li class="access"><a href="/company/access/">アクセス</a></li>
                    <li class="csr"><a href="/company/csr/">CSR</a></li>
                    <li class="privacy"><a href="/company/privacy/">プライバシーポリシー</a></li>
                  </ul>
                </li>
                <li class="technique"><a href="/technique/"><span>技 術</span></a>
                  <ul>
                    <li><a href="/technique/">組版・制作</a></li>
                    <li class="proof"><a href="/technique/proof/">色校正</a></li>
                    <li class="printing"><a href="/technique/printing/">印刷</a></li>
                    <li class="process"><a href="/technique/process/">製本・加工</a></li>
                    <li class="trobleshooting"><a href="/technique/trobleshooting/">トラブル解決</a></li>
                    <li class="case"><a href="/technique/case/">製品事例</a></li>
                  </ul>
                </li>
              </ul>
            </div>
            <div class="col">
              <ul>
                <li class="service"><a href="/service/"><span>サービス</span></a></li>
<?php /*                
                <li class="works"><a href="/works/"><span>実 績</span></a></li>
                <li class="fukuinfo"><a href="/fukuinfo/"><span>FUKUINFO</span></a></li>
			*/
?>								
                <li class="recruit"><a href="/recruit/"><span>採用情報</span></a>
                  <ul>
                    <li class=""><a href="/recruit/">採用情報</a></li>
                    <li class=""><a href="/recruit/contact/">採用に関するお問い合わせ</a></li>
                  </ul>
                </li>
                <li class="contact"><a href="/contact/"><span>お問い合わせ</span></a></li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </main>
<?php
	get_footer();
?>