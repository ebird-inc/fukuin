<?php
	define('TMPL_ADMIN', 'to_admin.txt' );		//メール本文
	define('TMPL_USER', 'to_user.txt' );		//メール本文
	define('REV_FROM_NAME', '株式会社フクイン' );	//送信者名

	define('REV_FROM', 'info@fukuin.co.jp');	//送信元アドレス / 管理者送信先
	define('REV_TO', 'susumu_yoshikawa@fukuin.co.jp,t_kimura@fukuin.co.jp,y_ueta@fukuin.co.jp,r_takahashi@fukuin.co.jp,m_ishizuka@fukuin.co.jp');	//送信元アドレス / 管理者送信先
	define('REV_ERROR', 't_kimura@fukuin.co.jp');	//エラーメール送信先

	date_default_timezone_set('Asia/Tokyo');

	if ( isset($_REQUEST["comp"])) {
		//管理者へメール送信処理
		$MailSub_FromName=REV_FROM_NAME;
		$MailSub_From=REV_FROM;
		$MailSub_Return=REV_ERROR;
		$MailSub_To=REV_TO;
		$MailSub_Bcc = '';
		$MailSub_Subject="webから採用に関するお問い合わせが届いています";
	    $MailSub_Message = file_get_contents( TMPL_ADMIN );

	    $tags = array( '%%SEI_NAMAE%%', '%%MEI_NAMAE%%', '%%SEI_KANA%%', '%%MEI_KANA%%', '%%COMPANY%%', '%%TEL%%', '%%MAIL%%',
						'%%ZIP1%%', '%%ZIP2%%', '%%PREF%%', '%%ADDRESS1%%', '%%ADDRESS2%%', '%%TEL%%',
						'%%TYPE%%', '%%BODY%%' );
	    $replace = array( $_REQUEST['sei_namae'], $_REQUEST['mei_namae'], $_REQUEST['sei_kana'], $_REQUEST['mei_kana'], $_REQUEST['company'], $_REQUEST['tel'], $_REQUEST['mail'],
						$_REQUEST['zip1'], $_REQUEST['zip2'], $_REQUEST['pref'], $_REQUEST['address1'], $_REQUEST['address2'], $_REQUEST['tel'],
						$_REQUEST['type'], preg_replace( '/<br>/', "\r\n", $_REQUEST['body'] ) );
	    $MailSub_Message = str_replace( $tags, $replace, $MailSub_Message );

		$mlret=_mailSub($MailSub_FromName,$MailSub_From,$MailSub_To,$MailSub_Return,$MailSub_Bcc,$MailSub_Subject,$MailSub_Message);


		//申込者へメール送信処理
		$MailSub_FromName=REV_FROM_NAME;
		$MailSub_From=REV_FROM;
		$MailSub_To = $_REQUEST['mail'];
		$MailSub_Subject="【株式会社フクイン】採用に関するお問い合わせありがとうございました";
		$MailSub_Message = file_get_contents( TMPL_USER );

	    $MailSub_Message = str_replace( $tags, $replace, $MailSub_Message );

		$mlret=_mailSub($MailSub_FromName,$MailSub_From,$MailSub_To,$MailSub_Return,$MailSub_Bcc,$MailSub_Subject,$MailSub_Message);


		//完了画面へ遷移
		header( 'Location: ./comp.php' );

		exit();
	}

	function _mailSub( $MailSub_FromName, $MailSub_From, $MailSub_To, $MailSub_Return, $MailSub_Bcc, $MailSub_Subject, $MailSub_Message ) {
		global $file, $fileName;

		$MailSub_Bcc_Array=explode( "<>", $MailSub_Bcc );

		// 念の為、言語と文字コードの設定
		mb_language("Japanese");
		mb_internal_encoding("UTF-8");
		//mb_detect_order("SJIS, EUC-JP, ASCII, JIS, UTF-8");
		//ヘッダ編集

		// 送信元情報をエンコード
		$from_addr = REV_FROM;
		$from_name_enc = mb_encode_mimeheader( REV_FROM_NAME, "ISO-2022-JP" );
		$from = "$from_name_enc<$from_addr>";
		// メールヘッダを作成
		$MailSub_Header  = "From: $from\r\n";
		// Bcc
		$MailSub_Bcc_Array_cnt=0;
		foreach($MailSub_Bcc_Array as $MailSub_Bcc_Array_k=> $MailSub_Bcc_Array_v){
			if($MailSub_Bcc_Array_v!=""){
				$MailSub_Bcc_Array_cnt++;
				if($MailSub_Bcc_Array_cnt==1){
					$MailSub_Header.= 'Bcc: '.$MailSub_Bcc_Array_v;
				} else {
					$MailSub_Header.= ', '.$MailSub_Bcc_Array_v;
				}
			}
		}
		if($MailSub_Bcc_Array_cnt>=1){ $MailSub_Header.="\r\n"; }
		$MailSub_Header .= "Reply-To: $from\r\n";

		//メール を送信
		$MailSub_Err=mb_send_mail($MailSub_To, $MailSub_Subject, $MailSub_Message, $MailSub_Header, '-f ' . $MailSub_Return);


		return $MailSub_Err;
	}
?>
<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "contact",
		"css" => array(
			"basic",
			"contact",
		),
		"title" => "採用のお問い合わせ｜採用情報｜株式会社フクイン",
		"description" => "株式会社フクインの採用に関するお問い合わせのページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Recruit</p>
        <p>採用情報</p>
      </div>
      <nav class="breadcrumb">
        <ol>
          <li><a href="/">TOP</a></li>
          <li><a href="/recruit/">採用情報</a></li>
          <li><a href="/recruit/contact/">採用に関するお問い合わせ</a></li>
        </ol>
      </nav>
    </div>
    <main>
      <div id="wrapper">
        <div class="site">
          <section id="contact" class="conf">
            <header>
              <h2>採用に関するお問い合わせ</h2>
            </header>
            <p>この内容でよろしければ送信ボタンを押してください。<br>
              修正する場合は戻るボタンで戻り修正してください。</p>
            <form name="mycfm" id="mycfm" method="POST">
              <dl>
                <dt>お名前</dt>
                <dd><?php echo htmlspecialchars( $_REQUEST['sei_namae'] ) ?> <?php echo htmlspecialchars( $_REQUEST['mei_namae'] ) ?></dd>
                <dt>ふりがな</dt>
                <dd><?php echo htmlspecialchars( $_REQUEST['sei_kana'] ) ?> <?php echo htmlspecialchars( $_REQUEST['mei_kana'] ) ?></dd>
                <dt>会学校名/会社名</dt>
                <dd><?php echo htmlspecialchars( $_REQUEST['company'] ) ?></dd>
                <dt>メールアドレス</dt>
                <dd><?php echo htmlspecialchars( $_REQUEST['mail'] ) ?></dd>
                <dt>ご住所</dt>
                <dd>〒 <?php echo htmlspecialchars( $_REQUEST['zip1'] ) ?> - <?php echo htmlspecialchars( $_REQUEST['zip2'] ) ?><br>
                  <?php echo htmlspecialchars( $_REQUEST['pref'] ) ?> <?php echo htmlspecialchars( $_REQUEST['address1'] ) ?> <?php echo htmlspecialchars( $_REQUEST['address2'] ) ?></dd>
                <dt>お電話番号</dt>
                <dd><?php echo htmlspecialchars( $_REQUEST['tel'] ) ?></dd>
                <dt>お問い合わせの種類</dt>
                <dd><?php echo htmlspecialchars( $_REQUEST['type'] ) ?></dd>
                <dt>お問い合わせ内容</dt>
                <dd><?php echo nl2br( htmlspecialchars( $_REQUEST['body'] ) ) ?></dd>
              </dl>
              <div class="action">
                <button type="submit" class="btn">この内容で送信する</button>
                <button type="button" class="btn back" onclick="document.mycfm.action='./'; document.mycfm.submit();">前の画面に戻る</button>
              </div>
<input type="hidden" name="sei_namae" value="<?=htmlspecialchars( $_REQUEST['sei_namae'] ) ?>">
<input type="hidden" name="mei_namae" value="<?=htmlspecialchars( $_REQUEST['mei_namae'] ) ?>">
<input type="hidden" name="sei_kana" value="<?=htmlspecialchars( $_REQUEST['sei_kana'] ) ?>">
<input type="hidden" name="mei_kana" value="<?=htmlspecialchars( $_REQUEST['mei_kana'] ) ?>">
<input type="hidden" name="company" value="<?=htmlspecialchars( $_REQUEST['company'] ) ?>">
<input type="hidden" name="mail" value="<?=htmlspecialchars( $_REQUEST['mail'] ) ?>">
<input type="hidden" name="mail_cnf" value="<?=htmlspecialchars( $_REQUEST['mail_cnf'] ) ?>">
<input type="hidden" name="zip1" value="<?=htmlspecialchars( $_REQUEST['zip1'] ) ?>">
<input type="hidden" name="zip2" value="<?=htmlspecialchars( $_REQUEST['zip2'] ) ?>">
<input type="hidden" name="pref" value="<?=htmlspecialchars( $_REQUEST['pref'] ) ?>">
<input type="hidden" name="address1" value="<?=htmlspecialchars( $_REQUEST['address1'] ) ?>">
<input type="hidden" name="address2" value="<?=htmlspecialchars( $_REQUEST['address2'] ) ?>">
<input type="hidden" name="tel" value="<?=htmlspecialchars( $_REQUEST['tel'] ) ?>">
<input type="hidden" name="body" value="<?=htmlspecialchars( $_REQUEST['body'] ) ?>">
<input type="hidden" name="type" value="<?=htmlspecialchars( $_REQUEST['type'] ) ?>">
<input type="hidden" name="chk" value="<?=htmlspecialchars( $_REQUEST['chk'] ) ?>">

<input type="hidden" name="goback" value="1">
<input type="hidden" name="comp" value="1">
            </form>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>