<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "recruit-detail",
		"css" => array(
			"basic",
			"recruit",
		),
		"title" => "募集要項（中途）｜採用情報｜株式会社フクイン",
		"description" => "株式会社フクインの中途の募集要項のページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Recruit</p>
        <p>採用情報</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/recruit/">採用情報</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>FUKUINの採用</h1>
            <p>We will inform you of recruitment information.</p>
          </header>
          <section id="career">
            <header>
              <h2>中途採用</h2>
            </header>
<?php
	$args = array(
		'posts_per_page' => -1,
		'post_type' => array( 'career' ),
		'post_status' => array( 'publish' ),
		//'orderby' => 'menu_order',
		//'order' => 'ASC',
	);
	query_posts( $args );

	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();

			$kind = get_field( 'kind' );
			$contents = get_field( 'contents' );
			$requirements = html_paragraph( get_field( 'requirements' ) );
			$personality = html_paragraph( get_field( 'personality' ) );
			$process = html_paragraph( get_field( 'process' ) );
			$application = get_field( 'application' );
			$submission = html_paragraph( get_field( 'submission' ) );

			$employment = get_field( 'employment' );
			$salary = html_paragraph( get_field( 'salary' ) );
			$address = html_paragraph( get_field( 'address' ) );
			$working_hours = html_paragraph( get_field( 'working_hours' ) );
			$holiday = html_paragraph( get_field( 'holiday' ) );
			$insurance = html_paragraph( get_field( 'insurance' ) );
			$welfare = html_paragraph( get_field( 'welfare' ) );
			$others = html_paragraph( get_field( 'others' ) );
?>
            <section>
              <h3 class="h4">募集要項</h3>
              <dl>
			<?php if ( !empty( $kind ) ) { ?>
                <dt>募集職種</dt>
                <dd><?php echo $kind; ?></dd>
			<?php } ?>
			<?php if ( !empty( $contents ) ) { ?>
                <dt>仕事の内容</dt>
                <dd><?php echo $contents; ?></dd>
			<?php } ?>
			<?php if ( !empty( $requirements ) ) { ?>
                <dt>応募資格</dt>
                <dd><?php echo $requirements; ?></dd>
			<?php } ?>
			<?php if ( !empty( $personality ) ) { ?>
                <dt>求める人物像</dt>
                <dd><?php echo $personality; ?></dd>
			<?php } ?>
			<?php if ( !empty( $process ) ) { ?>
                <dt>選考過程</dt>
                <dd><?php echo $process; ?></dd>
			<?php } ?>
			<?php if ( !empty( $application ) ) { ?>
                <dt>応募書類</dt>
                <dd><?php echo $application; ?></dd>
			<?php } ?>
			<?php if ( !empty( $submission ) ) { ?>
                <dt>書類提出先</dt>
                <dd><?php echo $submission; ?></dd>
			<?php } ?>
              </dl>
            </section>
            <section>
              <h3 class="h4">中途採用データ</h3>
              <dl>
			<?php if ( !empty( $employment ) ) { ?>
                <dt>雇用形態</dt>
                <dd><?php echo $employment; ?></dd>
			<?php } ?>
			<?php if ( !empty( $salary ) ) { ?>
                <dt>初任給</dt>
                <dd><?php echo $salary; ?></dd>
			<?php } ?>
			<?php if ( !empty( $address ) ) { ?>
                <dt>勤務地</dt>
                <dd><?php echo $address; ?></dd>
			<?php } ?>
			<?php if ( !empty( $working_hours ) ) { ?>
                <dt>勤務時間</dt>
                <dd><?php echo $working_hours; ?></dd>
			<?php } ?>
			<?php if ( !empty( $holiday ) ) { ?>
                <dt>休日休暇</dt>
                <dd><?php echo $holiday; ?></dd>
			<?php } ?>
			<?php if ( !empty( $insurance ) ) { ?>
                <dt>加入保険</dt>
                <dd><?php echo $insurance; ?></dd>
			<?php } ?>
			<?php if ( !empty( $welfare ) ) { ?>
                <dt>福利厚生</dt>
                <dd><?php echo $welfare; ?></dd>
			<?php } ?>
			<?php if ( !empty( $others ) ) { ?>
                <dt>その他</dt>
                <dd><?php echo $others; ?></dd>
			<?php } ?>
              </dl>
            </section>
<?php endwhile; ?>
            <nav class="action">
              <p><a href="/recruit/contact/" class="btn">採用に関するお問い合わせ</a></p>
            </nav>
<?php else: ?>
            <p>現在募集しておりません</p>
<?php endif; ?>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>