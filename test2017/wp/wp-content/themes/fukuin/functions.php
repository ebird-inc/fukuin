<?php
//Pagenation
function pagination( $pages='', $range=2 ) {
     global $paged;	//現在のページ値
     if( empty( $paged ) ) $paged = 1;	//デフォルトのページ

     if( $pages == '' ) {
         global $wp_query;
         $pages = $wp_query->max_num_pages;	//全ページ数を取得
         if( !$pages ) $pages = 1;	//全ページ数が空の場合は、１とする

		$max = $wp_query->max_num_pages;
     }

	if ($paged == 1 || ( $paged == $max ) ) {
		$range = 4;
	} elseif ($paged == 2 || ( ($paged+1) == $max )) {
		$range = 3;
	} else {
	}
     $showitems = ( $range ) + 1;	//表示するページ数（５ページを表示）

     if ( 1 != $pages ) {	//全ページが１でない場合はページネーションを表示する
		 echo "<ul>\n";
		 //Prev：現在のページ値が１より大きい場合は表示
         if( $paged > 1 ) echo '<li class="prev"><a href="' . get_pagenum_link( $paged - 1 ) . '"><span class="icon"><</span></a></li>' . "\n";
         //if( $paged > 1 ) echo "<li class=\"prev\"><a href='" . get_pagenum_link( 1 ) . "'>&laquo;</a></li>\n";

         for ( $i=1; $i <= $pages; $i++ ) {
             if ( 1 != $pages &&( !( $i >= $paged + $range+1 || $i <= $paged - $range-1 ) || $pages <= $showitems ) ) {
                //三項演算子での条件分岐
                echo ( $paged == $i )? '<li><span>' . $i . '</span></li>' . "\n":'<li><a href="' . get_pagenum_link( $i ) . '">' . $i . '</a></li>' . "\n";
             }
         }
		//Next：総ページ数より現在のページ値が小さい場合は表示
		if ( $paged < $pages ) echo '<li class="next"><a href="' . get_pagenum_link( $paged + 1 ) . '"><span class="icon">></span></a></li>' . "\n";
		//if ( $paged < $pages ) echo "<li class=\"next\"><a href=\"" . get_pagenum_link( $pages ) . "\">&raquo;</a></li>\n";
		echo "</ul>\n";
	 } else {
		 echo "<ul></ul>";
     }
}

// pタグ改行置き換え(カスタムフィールド　textarea用)
function html_paragraph($str, $xhtml=true){
    $arr = preg_split("/\R\R+/", $str, -1, PREG_SPLIT_NO_EMPTY);
    $result = "";
    foreach($arr as $value){
        $result .= nl2br(strip_tags($value,'<img><a><h1><h2><h3><h4><h5><strong>'), $xhtml);
    }
    return $result;
}

// pタグ改行置き換え(投稿用)
function html_paragraph_normal( $str ) {
    $arr = preg_split( '/\R\R+/', $str, -1, PREG_SPLIT_NO_EMPTY );
    $result = '';
    foreach( $arr as $value ) {
        $result .= strip_tags( $value, '<img><a><h1><h2><h3><h4><h5><strong>' ) . '<br />';
    }
    return $result;
}


//本文抜粋を取得する関数
function get_the_custom_excerpt($content, $length, $append='...' ) {
	$length = ($length ? $length : 70);
	$content =  preg_replace('/<!--more-->.+/is',"",$content); //moreタグ以降削除
	$content =  strip_shortcodes($content);//ショートコード削除
	$content =  strip_tags($content);//タグの除去
	$content =  str_replace("&nbsp;","",$content);//特殊文字の削除（今回はスペースのみ）
	$content =  mb_strimwidth($content,0,$length, $append, 'UTF-8');//文字列を指定した長さで切り取る
	return $content;
}

//第一引数・・・調べたい配列
//第二引数・・・基準となる値
//返り値・・・調べたい配列の、基準となる値の前後の値が返る。なければブランク
function _next( $array, $value ) {
    $tmp = '';
    foreach( $array as $v ) {
        if( $v == $value ) return $tmp;
        $tmp = $v;
    }

	//一致しない時
    $tmp = '';
    foreach( $array as $v ) {
        if( $v < $value ) {
            return $tmp;
        } else {
        	$tmp = $v;
        }
    }

    return '';
}
function _prev( $array, $value ) {
    $tmp = '';
    foreach( $array as $v ) {
        if( $v == $value ) {
            $tmp = $v;
        } elseif( $tmp !== '' ) {
            return $v;
        }
    }

    return '';
}

function getPostImage( $body, $mypost ) {
	if( empty( $mypost ) ) return null;

	if( ereg( '<img([ ]+)([^>]*)src\=["|\']([^"|^\']+)["|\']([^>]*)>', $body, $img_array ) ) {
		// imgタグで直接画像にアクセスしているものがある
		$dummy=ereg( '<img([ ]+)([^>]*)alt\=["|\']([^"|^\']+)["|\']([^>]*)>', $body, $alt_array );
		$resultArray[ 'url' ] = $img_array[ 3 ];
		$resultArray[ 'alt' ] = $alt_array[ 3 ];
	} else {
		// 直接imgタグにてアクセスされていない場合は紐づけられた画像を取得
		$files = get_children( array( 'post_parent' => $mypost->ID, 'post_type' => 'attachment', 'post_mime_type' => 'image' ) );
		if( is_array( $files ) && count( $files ) != 0 ) {
			$files = array_reverse( $files );
			$file = array_shift( $files );
			$resultArray[ 'url' ] = wp_get_attachment_url( $file->ID );
			$resultArray[ 'alt' ] = $file->post_title;
		} else {
			return null;
		}
	}
	return $resultArray;
}

// 使用しないメニューを非表示にする
function remove_admin_menus() {
	global $menu;

		// level10以外のユーザーの場合
		//unset($menu[2]);	// ダッシュボード
        unset($menu[5]);	// 投稿
        unset($menu[10]);	// メディア
        unset($menu[20]);	// 固定ページ
        unset($menu[25]);	// コメント
        //////unset($menu[60]);	// 外観
        //////unset($menu[65]);	// プラグイン
        //////unset($menu[70]);	// ユーザー
        //////unset($menu[75]);	// ツール
        //////unset($menu[80]);	// 設定

}
add_action('admin_menu', 'remove_admin_menus');

// 管理バーの使用しないメニューを非表示にする
function remove_admin_bar_menu( $wp_admin_bar ) {
	$wp_admin_bar->remove_node('wp-logo'); // Wpロゴ
	$wp_admin_bar->remove_node('comments'); // コメント
	$wp_admin_bar->remove_node('new-content'); // 新規投稿ボタン
	/* 管理バー右の部分 */
	//$wp_admin_bar->remove_node('edit-profile'); // プロフィール編集
	//$wp_admin_bar->remove_node('user-info'); // ユーザー
}
add_action( 'admin_bar_menu', 'remove_admin_bar_menu', 99 );

//クイック編集削除
add_filter( 'post_row_actions', 'hide_quickedit' );
function hide_quickedit($actions){
	unset($actions['inline hide-if-no-js']);
	return $actions;
}

// 投稿画面の項目を非表示にする
function remove_menu() {
	global $current_user;
    //例　管理者１以外は非表示を実行する
    //if($current_user->data->user_login === '管理者１') {
        remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=post_tag');//投稿タグ
        remove_submenu_page('edit.php', 'edit-tags.php?taxonomy=category');//投稿カテゴリー
        remove_submenu_page('edit.php?post_type=カスタム投稿タイプ', 'edit-tags.php?taxonomy=カスタム投稿タイプタクソノミー&amp;post_type=カスタム投稿タイプ');//カスタム投稿 タクソノミー
        remove_menu_page('edit.php?post_type=page'); // 固定ページ
        remove_menu_page('upload.php'); // メディア
        remove_menu_page('link-manager.php'); // リンク
        remove_menu_page('edit-comments.php'); // コメント
        //remove_menu_page('themes.php'); // 外観
        //remove_menu_page('plugins.php'); // プラグイン
        //remove_menu_page('users.php'); // ユーザー
        //remove_menu_page('tools.php'); // ツール
        //remove_menu_page('options-general.php'); // 設定
        //remove_menu_page('edit.php?post_type=acf'); //Advanced Custom Field（プラグイン）
        //remove_menu_page('edit.php?post_type=smart-custom-fields'); //Smart Custom Field（プラグイン）
        //remove_menu_page('cptui_main_menu'); //CPT UI（プラグイン）
    //}
    //例　権限が投稿者、寄稿者にはプロフィールメニューを出さない。
    /*if($current_user->caps['contributor'] || $current_user->caps['editor']) {
        remove_menu_page('profile.php'); // プロフィール
    }*/
}
add_action('admin_menu', 'remove_menu', 11);

/*-------------------------------------------*/
/*	管理画面の更新通知 無効化
/*-------------------------------------------*/
// WordPress本体の更新通知を無効化
add_filter('pre_site_transient_update_core', '__return_zero');
remove_action('wp_version_check', 'wp_version_check');
remove_action('admin_init', '_maybe_update_core');

// プラグインの更新通知を無効化
remove_action( 'load-update-core.php', 'wp_update_plugins' );
add_filter( 'pre_site_transient_update_plugins', create_function( '$a', "return null;" ) );

// テーマの更新通知を無効化
remove_action( 'load-update-core.php', 'wp_update_themes' );
add_filter( 'pre_site_transient_update_themes', create_function( '$a', "return null;" ) );

// 管理メニューから「更新」を消す
function remove_admin_menu_items() {
    remove_submenu_page('index.php','update-core.php');
}
add_action('admin_menu','remove_admin_menu_items');
