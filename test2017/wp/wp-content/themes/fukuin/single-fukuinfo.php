<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "fukuinfo-detail",
		"css" => array(
			"basic",
			"single",
			"fukuinfo",
		),
		"title" => get_field('title2') . "：FUKUINFO｜株式会社フクイン",
		"description" => get_field('description'),
		"og_image" => get_field('list_image'),
	));
?>
    <div id="visual">
      <div class="site">
        <p>FUKUINFO</p>
        <p>フクインフォ</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/fukuinfo/">FUKUINFO</a></li>
<?php
	$ctg = ''; $ctg_name = '';
	if ( $terms = get_the_terms( $post->ID, 'ctg' ) ) {
		foreach ( $terms as $term ) {
			$ctg = esc_html( $term->slug );
			$ctg_name = esc_html( $term->name );
			break;
		}
	}
	$ct_args = array(
		'hide_empty' => false	// 投稿記事がないタームも取得
	);

	if ( !empty( $ctg ) ) {
?>
		<li><a href="/fukuinfo/?ctg=<?php echo $term->slug; ?>"><?php echo $ctg_name; ?></a></li>
<?php
	}
?>
		<li><?php the_field('title2');?></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
        	<nav class="category">
				<ul>
					<li<?php if ( $ctg == '' ) echo ' class="is-active"'; ?>><a href="/fukuinfo"><span>ALL</span></a></li>
<?php
	$terms = get_terms( 'ctg' , $ct_args );
	if ( count( $terms ) > 0 ) {
		foreach ( $terms as $term ) {
			$active = '';

	        $term = sanitize_term( $term, $taxonomy );
	        $term_link = get_term_link( $term, $taxonomy );
	        if ( is_wp_error( $term_link ) ) continue;

        	if( !$term->parent ) {	// 親タームの場合
				if ( $ctg == $term->slug ) $active = ' class="is-active"';
?>
					<li<?php echo $active; ?>><a href="/fukuinfo/?ctg=<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
<?php
			}
		}
	}

	$title1 = get_field('title1');
	$title2 = get_field('title2');
?>
				</ul>
			</nav>
			<article>
				<header>
					<p class="date"><?php the_field('mdate');?></p>
					<p class="category"><?php echo $ctg_name; ?></p>
					<?php if ( !empty( $title1 ) ) { ?>
					<p class="subtitle"><?php echo $title1; ?></p>
					<?php } ?>
					<?php if ( !empty( $title2 ) ) { ?>
					<p class="title"><?php echo $title2; ?></p>
					<?php } ?>
				</header>
				<div class="content">
<?php
	if( have_rows('body_content') ): //柔軟コンテンツフィールド
		$cnt = 0;
		while ( have_rows('body_content') ) : the_row();
			if( get_row_layout() == 'body_layout' ):
				if ( $cnt == 0 ) {
?>
					<h2><?php the_sub_field('body_title');?></h2>
<?php			} else { ?>
					<h3><?php the_sub_field('body_title');?></h3>
<?php			} ?>
					<p><?php the_sub_field('body');?></p>
<?php			$cnt += 1;
			endif;
		endwhile;
	endif;
?>
				</div>
			</article>
			<nav class="pager">
<?php
	$ids = array();
	$cur_id = get_the_ID();
	$args = array(
		'post_type' => array( 'fukuinfo' ),
		'orderby' => 'menu_order',
		'order' => 'ASC',
		'posts_per_page' => -1,
	);
	query_posts( $args );
	if ( have_posts() ) {
		while ( have_posts() ) {
			the_post();
			$ids[] = $post->ID;
			$arr[ $post->ID ] = get_field( 'title2' );
		}
	}

	$prev_id = _prev( $ids, $cur_id );
	$next_id = _next( $ids, $cur_id );

	wp_reset_query();
?>
				<ul>
				<?php if ( !empty( $prev_id ) ) { ?>
					<li class="prev"><a href="/fukuinfo/<?php echo $prev_id; ?>"><?php echo $arr[ $prev_id ]; ?></a></li>
				<?php } else { ?>
					<li class="prev"></li>
				<?php } ?>
				<?php if ( !empty( $next_id ) ) { ?>
					<li class="next"><a href="/fukuinfo/<?php echo $next_id; ?>"><?php echo $arr[ $next_id ]; ?></a></li>
				<?php } else { ?>
					<li class="next"></li>
				<?php } ?>
				</ul>
			</nav>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>
