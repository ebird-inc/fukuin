<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	$wks_name = '';
	$ctg = htmlspecialchars( $_GET[ 'wks_ctg' ] );
	if ( !empty( $ctg ) ) {
		$wks = get_term_by( 'slug', $ctg, 'wks_ctg' );
		if ( $wks ) $wks_name = $wks->name . '：';
	}

	get_header_html(array(
		"body_class" => "works",
		"css" => array(
			"basic",
			"archive",
			"works",
		),
		"title" => $wks_name . "実績｜株式会社フクイン",
		"description" => "",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Works</p>
        <p>実 績</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/works/">実績</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
        	<nav class="category">
        	  <ul>
<?php
	$ct_args = array(
		'hide_empty' => false	// 投稿記事がないタームも取得
	);
?>
					<li<?php if ( empty( $ctg ) ) echo ' class="is-active"'; ?>><a href="/works"><span>ALL</span></a></li>
<?php
	$terms = get_terms( 'wks_ctg' , $ct_args );
	if ( count( $terms ) > 0 ) {
		foreach ( $terms as $term ) {
			$active = '';

	        $term = sanitize_term( $term, $taxonomy );
	        $term_link = get_term_link( $term, $taxonomy );
	        if ( is_wp_error( $term_link ) ) continue;

        	if( !$term->parent ) {	// 親タームの場合
				if ( $ctg == $term->slug ) $active = ' class="is-active"';
?>
					<li<?php echo $active; ?>><a href="/works/?wks_ctg=<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
<?php
			}
		}
	}
?>
						</ul>
					</nav>
	        <div class="sidefull">
						<section id="list">
							<ul>
<?php
    //$list_max_num = get_field( 'list_max_num', 'option' );
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	if ( !empty( $ctg ) ) {
		$args = array(
			'posts_per_page' => 6,
			'tax_query' => array( //タクソノミー、タームの設定
				array(
					'taxonomy' => 'wks_ctg',
					'field' => 'slug',
					'terms' => $ctg,
				),
			),
			'paged' => $paged,
			'post_type' => array( 'works' ),
			'post_status' => array( 'publish' ),
			'orderby' => 'menu_order',
			'order' => 'ASC',
		);
	} else {
		$args = array(
			'posts_per_page' => 6,
			'paged' => $paged,
			'post_type' => array( 'works' ),
			'post_status' => array( 'publish' ),
			'orderby' => 'menu_order',
			'order' => 'ASC',
		);
	}
	query_posts( $args );

	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
			if ( $terms = get_the_terms( $post->ID, 'wks_ctg' ) ) {
				foreach ( $terms as $term ) {
					$term_name = esc_html( $term->name );
					$term_slug = esc_html( $term->slug );
					break;
				}
			}

			$title1 = get_field( 'title1' );
			$title2 = get_field( 'title2' );
?>
								<li><a href="<?php echo esc_url( get_permalink() ); ?>">
									<figure><img src="<?php echo get_field( 'list_image' ); ?>" atl=""></figure>
									<div class="container">
										<p class="date"><?php echo get_field( 'mdate' ); ?></p>
										<p class="category"><?php echo $term_name; ?></p>
										<?php if ( !empty( $title1 ) ) { ?>
										<p class="subtitle"><?php echo $title1; ?></p>
										<?php } ?>
										<?php if ( !empty( $title2 ) ) { ?>
										<p class="title"><?php echo $title2; ?></p>
										<?php } ?>
									</div>
									</a></li>
<?php
     endwhile;
endif;
?>
							</ul>
						</section>
						<nav class="pagination">
<?php
	if ( function_exists( 'pagination' ) ) pagination( $max_num_pages );
?>
						</nav>
					</div>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>