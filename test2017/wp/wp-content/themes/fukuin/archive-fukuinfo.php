<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	$wks_name = '';
	$ctg = htmlspecialchars( $_GET[ 'ctg' ] );
	if ( !empty( $ctg ) ) {
		$wks = get_term_by( 'slug', $ctg, 'ctg' );
		if ( $wks ) $wks_name = $wks->name . '：';
	}

	get_header_html(array(
		"body_class" => "fukuinfo",
		"css" => array(
			"basic",
			"archive",
			"fukuinfo",
		),
		"title" => $wks_name . "FUKUINFO｜株式会社フクイン",
		"description" => "",
	));
?>
    <div id="visual">
      <div class="site">
        <p>FUKUINFO</p>
        <p>フクインフォ</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/fukuinfo/">FUKUINFO</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
        	<nav class="category">
        	  <ul>
<?php
	$ctg = htmlspecialchars( $_GET[ 'ctg' ] );
	$ct_args = array(
		'hide_empty' => false	// 投稿記事がないタームも取得
	);
?>
					<li<?php if ( empty( $ctg ) ) echo ' class="is-active"'; ?>><a href="/fukuinfo"><span>ALL</span></a></li>
<?php
	$terms = get_terms( 'ctg' , $ct_args );
	if ( count( $terms ) > 0 ) {
		foreach ( $terms as $term ) {
			$active = '';

	        $term = sanitize_term( $term, $taxonomy );
	        $term_link = get_term_link( $term, $taxonomy );
	        if ( is_wp_error( $term_link ) ) continue;

        	if( !$term->parent ) {	// 親タームの場合
				if ( $ctg == $term->slug ) $active = ' class="is-active"';
?>
					<li<?php echo $active; ?>><a href="/fukuinfo/?ctg=<?php echo $term->slug; ?>"><span><?php echo $term->name; ?></span></a></li>
<?php
			}
		}
	}
?>
						</ul>
					</nav>
	        <div class="sidefull">
						<section id="list">
							<ul>
<?php
    //$list_max_num = get_field( 'list_max_num', 'option' );
	$paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	if ( !empty( $ctg ) ) {
		$args = array(
			'posts_per_page' => 6,
			'tax_query' => array( //タクソノミー、タームの設定
				array(
					'taxonomy' => 'ctg',
					'field' => 'slug',
					'terms' => $ctg,
				),
			),
			'paged' => $paged,
			'post_type' => array( 'fukuinfo' ),
			'post_status' => array( 'publish' ),
			'orderby' => 'menu_order',
			'order' => 'ASC',
		);
	} else {
		$args = array(
			'posts_per_page' => 6,
			'paged' => $paged,
			'post_type' => array( 'fukuinfo' ),
			'post_status' => array( 'publish' ),
			'orderby' => 'menu_order',
			'order' => 'ASC',
		);
	}
	query_posts( $args );

	if ( have_posts() ) :
		while ( have_posts() ) :
			the_post();
			if ( $terms = get_the_terms( $post->ID, 'ctg' ) ) {
				foreach ( $terms as $term ) {
					$term_name = esc_html( $term->name );
					$term_slug = esc_html( $term->slug );
					break;
				}
			}

			$body = '';
			$title1 = get_field( 'title1' );
			$title2 = get_field( 'title2' );

			if( have_rows( 'body_content' ) ) { //柔軟コンテンツ
				while ( have_rows( 'body_content' ) ) : the_row();
					if( get_row_layout() == 'body_layout' ) {
						if ( $body == '' ) $body = get_the_custom_excerpt( get_sub_field( 'body' ), 120 );
					}
				endwhile;
			}
?>
								<li><a href="<?php echo esc_url( get_permalink() ); ?>">
									<figure><img src="<?php echo get_field( 'list_image' ); ?>" atl=""></figure>
									<div class="container">
										<p class="date"><?php echo get_field( 'mdate' ); ?></p>
										<p class="category"><?php echo $term_name; ?></p>
										<?php if ( !empty( $title1 ) ) { ?>
										<p class="subtitle"><?php echo $title1; ?></p>
										<?php } ?>
										<?php if ( !empty( $title2 ) ) { ?>
										<p class="title"><?php echo $title2; ?></p>
										<?php } ?>
										<p class="excerpt"><?php echo $body; ?></p>
										<p class="more">続きを読む</p>
									</div>
									</a></li>
<?php
     endwhile;
endif;
?>
							</ul>
						</section>
						<nav class="pagination">
<?php
	if ( function_exists( 'pagination' ) ) pagination( $max_num_pages );
?>
							<!--ul>
								<li><span>1</span></li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href="">4</a></li>
							</ul-->
						</nav>
					</div>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>