<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "works-detail",
		"css" => array(
			"basic",
			"single",
			"works",
		),
		"title" => "実績｜株式会社フクイン",
		"description" => "",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Works</p>
        <p>実 績</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/works/">実績</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
        	<nav class="category">
        	  <ul>
							<li class="is-active"><a href=""><span>ALL</span></a></li>
							<li><a href=""><span>書籍</span></a></li>
							<li><a href=""><span>カタログ・パンフレット</span></a></li>
							<li><a href=""><span>その他</span></a></li>
						</ul>
					</nav>
					<article>
						<header>
							<p class="date">2017.10.20</p>
							<p class="category">カタログ・パンフレット</p>
							<p class="subtitle">パイオニア様【カタログ】</p>
							<p class="title">高精細印刷で商品ディティール表現を再現</p>
						</header>
						<div class="content">
							<p><img src="../../assets/images/works/detail/tmp_img_01.jpg" alt=""></p>
							<p>クライアント：<a href="">パイオニア株式会社</a><br>
								企画制作：<a href="">株式会社ADKインターナショナル</a></p>
							<p>フクインの歴史は1948年福音電気（現　パイオニア株式会社）の創業者でもある松本望が福音印刷を創業したことに始まり、現在もパイオニア製品のカタログやマニュアルの多くが当社のデジタルワークフローで印刷されています。同社の製品カタログに向けては、株式会社ADKインターナショナル様と共同で高精細印刷ワークフローをご提案。筐体の素材感やヘアラインの質感向上、ブラック基調製品の暗い領域における黒の深みが増すことで、高級感のあるブラック表現を実現しています。スピーカーコーンや筐体では細かいメッシュ形状のひとつひとつのディティール再現性が向上しました。また、高精細印刷により彩度も向上し、中間領域における色域の広がりが、感動を持ったビジュアルとしての表現能力を発揮しています。<br>
							高精細ワークフローでは、製品のディスプレイ部では通常の画像補正のみで、ディスプレイが持つ高い輝度とコントラストを忠実に表現するミクロの世界の美しさを得ることが可能となります。キリヌキのエッジの柔らかさとともに、高精細印刷のもたらす多くのメリットはカタログ上のレイアウトで写真が小さくとも、ディティールを損なうことなく再現されています。<br><br></p>
								<p><img src="../../assets/images/works/detail/tmp_img_02.jpg" alt=""></p>
								<p>小さな画像でも各部のディティールは緻密に再現されています。<br><br></p>
								<p><img src="../../assets/images/works/detail/tmp_img_03.jpg" alt=""></p>
								<p><strong>1</strong> 緻密に表現されるスピーカーコーンや筐体のディティール<br>
								<strong>2</strong> 質感のあるヘアラインとシルバー表現<br>
								<strong>3</strong> シャープなディスプレイ部の表現</p>
						</div>       
					</article>  
					<nav class="pager">
						<ul>
							<li class="prev"><a href="">●●●●●について</a></li>
							<li class="next"><a href="">●●●●●について</a></li>
						</ul>
					</nav>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>