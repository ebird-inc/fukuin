<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "works",
		"css" => array(
			"basic",
			"archive",
			"works",
		),
		"title" => "実績｜株式会社フクイン",
		"description" => "",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Works</p>
        <p>実 績</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/works/">実績</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
        	<nav class="category">
        	  <ul>
							<li class="is-active"><a href=""><span>ALL</span></a></li>
							<li><a href=""><span>書籍</span></a></li>
							<li><a href=""><span>カタログ・パンフレット</span></a></li>
							<li><a href=""><span>その他</span></a></li>
						</ul>
					</nav>
	        <div class="sidefull">
						<section id="list">
							<ul>
								<li><a href="./detail/">
									<figure><img src="/assets/images/home/works_img.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.01.20</p>
										<p class="category">パンフレット</p>
										<p class="subtitle">パイオニア様【カタログ】</p>
										<p class="title">高精細印刷で商品ディティール表現を再現</p>
									</div>
									</a></li>
								<li><a href="">
									<figure><img src="/assets/images/home/works_img.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.01.20</p>
										<p class="category">パンフレット</p>
										<p class="subtitle">パイオニア様【カタログ】</p>
										<p class="title">高精細印刷で商品ディティール表現を再現</p>
									</div>
									</a></li>
								<li><a href="">
									<figure><img src="/assets/images/home/works_img.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.01.20</p>
										<p class="category">パンフレット</p>
										<p class="subtitle">パイオニア様【カタログ】</p>
										<p class="title">高精細印刷で商品ディティール表現を再現</p>
									</div>
									</a></li>
							</ul>
						</section>
						<nav class="pagination">
							<ul>
								<li><span>1</span></li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href="">4</a></li>
							</ul>
						</nav>						
					</div>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>