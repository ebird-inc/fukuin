<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "fukuinfo-detail",
		"css" => array(
			"basic",
			"single",
			"fukuinfo",
		),
		"title" => "FUKUINFO｜株式会社フクイン",
		"description" => "",
	));
?>
    <div id="visual">
      <div class="site">
        <p>FUKUINFO</p>
        <p>フクインフォ</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/fukuinfo/">FUKUINFO</a></li>
				<li><a href="./">モニターと印刷物の色の違い</a>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
        	<nav class="category">
        	  <ul>
							<li class="is-active"><a href=""><span>ALL</span></a></li>
							<li><a href=""><span>フクインの人</span></a></li>
							<li><a href=""><span>印刷の○○話</span></a></li>
							<li><a href=""><span>工場紹介</span></a></li>
							<li><a href=""><span>デザイン百科</span></a></li>
							<li><a href=""><span>フクインの人</span></a></li>
							<li><a href=""><span>印刷の○○話</span></a></li>
							<li><a href=""><span>工場紹介</span></a></li>
							<li><a href=""><span>デザイン百科</span></a></li>
						</ul>
					</nav>
					<article>
						<header>
							<p class="date">2017.10.20</p>
							<p class="category">印刷の○○話</p>
							<p class="title">モニターと印刷物の色の違い</p>
						</header>
						<div class="content">
							<h2>「印刷の種類と仕組み」</h2>
							<p>印刷を分類する場合、印刷物の原稿となる版の形状で分類するのが一般的です。<br>
								平版印刷、凸版印刷、凹版印刷、孔版印刷の4種類に分けられます。<br>
								日常生活でもっとも多く目にする書籍や雑誌、新聞、カタログ、ポスターなどの多くは平版印刷で作成されています。<br>
								なお、凸版、凹版、孔版印刷はインキを直接紙などに転写して印刷するのに対し、平版印刷は版に塗布したインキをいったんゴムや樹脂(ブランケットと呼ばれる)などに転写してから紙に転写します。<br>
								そのため、紙と版が直接接触しない独特なインキの転写方法をとることから、オフセット印刷とも呼ばれます。</p>
							<h3>―オフセット印刷―</h3>
							<p>印刷速度が早く、大部数、多色物に経済性があります。<br>
紙質を選ばず、修正も比較的容易ですが、仕上がりは凸版印刷に比べやや力強さに欠けます。</p>
							<p><strogn>[主な印刷物]</strogn> 新聞をはじめとする出版物、ポスター、チラシ等</p>
							<h3>版式の原理</h3>
							<p>オフセット印刷では水と油の反発を利用して印刷をおこなっています。<br>
オフセット印刷で使用される刷版（印刷の原稿となる版）は、アルミ板で作られたサーマルCTPプレートを使います。<br>
PS版の表面には感光剤が塗布してあり、レーザー光で感光させて現像をすると、画線部は親油性、画線以外の部分は親水性の異なる性質を持つようになります。<br>
そして、印刷をするために版にインキを塗布するときは、インキを塗布する前に水（湿し水）を塗布します。<br>
そうすると、親水性の性質を持つ画線以外の部分に水が残り、その状態に油性のインキを塗布すると、画線以外の部分は水によってインキがはじかれて塗布されるのを防ぎ、親油性を持つ画線部にのみインキが塗布されることになります。
							</p>
							<p><img src="../../assets/images/fukuinfo/detail/tmp_img_01.png" alt=""></p>							
							<p>版の厚さは数μなので、見た目には平らな板にしか見えません。なので、平版印刷と呼ばれます。</p>							
							<h3>印刷機の原理</h3>
							<p>塗布されたインキは紙に直接転写されるのではなく、弾力性のあるゴムや樹脂などでできたブランケット胴にいったん転写されてから(オフ)、圧力が加えられて紙に転写されます(セット！)。</p>						
							<p><img src="../../assets/images/fukuinfo/detail/tmp_img_02.png" alt=""></p>
							<h3>濃淡の表現と識別</h3>
							<p>印刷物の濃淡は網点の大小で表現され、インキの膜厚は網点の大きさに関係なく均一です。	</p>	
							<p><img src="../../assets/images/fukuinfo/detail/tmp_img_03.png" alt=""></p>
							<p><strong>＊網点とは</strong></p>																															
							<p><img src="../../assets/images/fukuinfo/detail/tmp_img_04.png" alt=""></p>
							<p>この４色のツブは網点といい、シアン、イエロー、マゼンタ、ブラックの４色です。<br>
カラー印刷物はこの４色の網点で表現されています。</p>
							<h3>―凸版印刷―</h3>	
							<p>印刷する画線部が凸状になった版を使用する印刷です。<br>
凸状になった部分にインキをつけ紙に直接押し当てるので版は逆像になります。<br>
身近なところではハンコがこれと同じ原理です。<br>
								印刷した画線部の輪郭が鮮明で、仕上がりも力強くなる特長がありますが、インキを紙に転写する際に圧力をかけるため紙に凹みが発生することや、広い範囲へのインキ転写には向かないなどのデメリットがあります。</p>
							<p><strong>[主な印刷物]</strong> 週刊マンガ等</p>
							<h3>版式の原理</h3>
							<p>凸版は画像部(インキが付くところ)が凸状、非画像部(インクが付かないところ)が凹状になっていて、印刷時は凸部にインキが付着し、印刷される仕組みになっています。</p>																												
							<p><img src="../../assets/images/fukuinfo/detail/tmp_img_05.png" alt=""></p>
							<h3>印刷機の原理</h3>
							<p>圧力を加えて版から直接紙、その他の被印刷体にインキを転写し印刷します。</p>
							<p><img src="../../assets/images/fukuinfo/detail/tmp_img_06.png" alt=""></p>				
						</div>       
					</article>  
					<nav class="pager">
						<ul>
							<li class="prev"><a href="">●●●●●について</a></li>
							<li class="next"><a href="">●●●●●について</a></li>
						</ul>
					</nav>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>