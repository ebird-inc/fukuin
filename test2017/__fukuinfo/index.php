<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "fukuinfo",
		"css" => array(
			"basic",
			"archive",
			"fukuinfo",
		),
		"title" => "FUKUINFO｜株式会社フクイン",
		"description" => "",
	));
?>
    <div id="visual">
      <div class="site">
        <p>FUKUINFO</p>
        <p>フクインフォ</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/fukuinfo/">FUKUINFO</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
        	<nav class="category">
        	  <ul>
							<li class="is-active"><a href=""><span>ALL</span></a></li>
							<li><a href=""><span>フクインの人</span></a></li>
							<li><a href=""><span>印刷の○○話</span></a></li>
							<li><a href=""><span>工場紹介</span></a></li>
							<li><a href=""><span>デザイン百科</span></a></li>
							<li><a href=""><span>フクインの人</span>/</a></li>
							<li><a href=""><span>印刷の○○話</span></a></li>
							<li><a href=""><span>工場紹介</span></a></li>
							<li><a href=""><span>デザイン百科</span></a></li>
						</ul>
					</nav>
	        <div class="sidefull">
						<section id="list">
							<ul>
								<li><a href="./detail/">
									<figure><img src="/assets/images/fukuinfo/tmp_img_01.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.10.25</p>
										<p class="category">工場紹介</p>
										<p class="subtitle">工場見学</p>
										<p class="title">株式会社メイリン</p>
										<p class="excerpt">豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
										<p class="more">続きを読む</p>
									</div>
									</a></li>
								<li><a href="">
									<figure><img src="/assets/images/fukuinfo/tmp_img_02.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.10.21</p>
										<p class="category">フクインの人</p>
										<p class="subtitle">代表取締役 藤原 俊一</p>
										<p class="title">トップインタビュー</p>
										<p class="excerpt">豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
										<p class="more">続きを読む</p>
									</div>
									</a></li>
								<li><a href="">
									<figure><img src="/assets/images/fukuinfo/tmp_img_03.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.10.20</p>
										<p class="category">印刷の○○話</p>
										<p class="title">モニターと印刷物の色の違い</p>
										<p class="excerpt">豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
										<p class="more">続きを読む</p>
									</div>
									</a></li>
								<li><a href="./detail/">
									<figure><img src="/assets/images/fukuinfo/tmp_img_04.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.10.18</p>
										<p class="category">印刷の○○話</p>
										<p class="title">モニターと印刷物の色の違い</p>
										<p class="excerpt">豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
										<p class="more">続きを読む</p>
									</div>
									</a></li>
								<li><a href="">
									<figure><img src="/assets/images/fukuinfo/tmp_img_05.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.10.11</p>
										<p class="category">印刷の○○話</p>
										<p class="title">モニターと印刷物の色の違い</p>
										<p class="excerpt">豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
										<p class="more">続きを読む</p>
									</div>
									</a></li>
								<li><a href="">
									<figure><img src="/assets/images/fukuinfo/tmp_img_06.jpg" atl=""></figure>
									<div class="container">
										<p class="date">2017.10.08</p>
										<p class="category">印刷の○○話</p>
										<p class="title">モニターと印刷物の色の違い</p>
										<p class="excerpt">豊富な自動処理機能とあらゆる業種に対応するカスタマイズ性を併せ持った、クラウドWEBデータベース「働くDB」。世にあまた...</p>
										<p class="more">続きを読む</p>
									</div>
									</a></li>									
							</ul>
						</section>
						<nav class="pagination">
							<ul>
								<li><span>1</span></li>
								<li><a href="">2</a></li>
								<li><a href="">3</a></li>
								<li><a href="">4</a></li>
							</ul>
						</nav>						
					</div>
        </div>
      </div>
    </main>    
<?php
	get_footer_html();
?>