<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "company-profile",
		"css" => array(
			"basic",
			"company",
		),
		"title" => "企業概要｜会社案内｜株式会社フクイン",
		"description" => "株式会社フクインの企業概要のページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Corporate Information</p>
        <p>会社案内</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/company/">会社案内</a></li>
        <li><a href="./">企業概要</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>企業概要</h1>
            <p>Company Profile</p>
          </header>
          <dl>
            <dt>商号</dt>
            <dd>株式会社フクイン</dd>
            <dt>本店</dt>
            <dd>東京都文京区音羽1丁目23番3号 </dd>
            <dt>電話</dt>
            <dd>本社 03-3946-2281(代) </dd>
            <dt>創業</dt>
            <dd>昭和23年7月</dd>
            <dt>設立</dt>
            <dd>昭和24年(1949年)12月15日 </dd>
            <dt>代表者</dt>
            <dd>代表取締役 藤原 俊一 </dd>
            <dt>取締役</dt>
            <dd>三室 覚<br>
              吉川 晋</dd>
            <dt>事業内容</dt>
            <dd>商業印刷、書籍出版印刷、マニュアル印刷、製本、DTP、企画・デザイン、組版・制作</dd>
            <dt>資本金</dt>
            <dd>7千2百万円 </dd>
            <dt>授権資本</dt>
            <dd>2億8千8百万円 </dd>
            <dt>決算日 </dt>
            <dd>8月31日 </dd>
            <dt>売上高</dt>
            <dd>17億7千2百万円(平成27年8月度) </dd>
            <dt>社員数</dt>
            <dd>49名(平成28年8月現在) </dd>
            <dt>事業所 </dt>
            <dd>
              <p>音羽第1ビル<br>
                〒112-0013 東京都文京区音羽1-23-3<br>
                工場 03-3946-2289(代) </p>
              <p>音羽第3ビル<br>
                〒112-0013 東京都文京区音羽1-24-7<br>
                総務 03-3946-2281(代) </p>
              <p>志賀ビル<br>
                〒112-0013 東京都文京区音羽1-23-22<br>
                志賀ビル1F </p>
            </dd>
            <dt>関連会社</dt>
            <dd>
              <p>フクイングラフィック株式会社<br>
                〒112-0013 東京都文京区音羽1-23-3<br>
                <a href="https://www.fukuin-graphic.com" target="_blank">https://www.fukuin-graphic.com</a></p>
              <p>株式会社メイリン<br>
                〒356-1151 埼玉県川越市今福1654-3<br>
                <a href="http://www.meirin.biz" target="_blank">http://www.meirin.biz</a></p>
              <p>フクイン・インターナショナル株式会社<br>
                <a href="http://fukuin-int.co.jp" target="_blank">http://fukuin-int.co.jp</a></p>
              <p> FUKUIN(Thailand) Co.,Ltd. </p>
            </dd>
            <dt>取引銀行</dt>
            <dd>三井住友銀行 大塚支店<br>
              みずほ銀行 大塚支店<br>
              三菱東京UFJ銀行 江戸川橋支店<br>
              東京都民銀行 神田支店<br>
              日本政策金融公庫 新宿支店<br>
            </dd>
          </dl>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>