<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "company",
		"css" => array(
			"basic",
			"company",
		),
		"title" => "社長あいさつ｜会社案内｜株式会社フクイン",
		"description" => "株式会社フクインの社長あいさつのページです。昭和二十三年、株式会社フクインは東京で印刷事業を創業してから、70年を迎えます。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Corporate Information</p>
        <p>会社案内</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/company/">会社案内</a></li>
        <li><a href="./">社長挨拶</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>社長挨拶</h1>
            <p>Message from the President</p>
          </header>
          <figure class="full"><img src="../assets/images/company/president.jpg" alt="代表取締役、藤原俊一"></figure>
          <p> 昭和二十三年、株式会社フクインは東京で印刷事業を創業してから、まもなく70年を迎えます。<br>
            長きにわたり、お客様より多くの挑戦の機会を頂きました。試行錯誤のくり返しではありましたが、お客様の期待に支えられながら今日を迎えることできました。<br>
            付加価値の高い印刷サービスを通じ、多くの人に喜びや豊かな生活、感動を味わっていただく、このことが我々の目指しているところであります。<br>
            しかし、最近のデジタル情報技術の進化は速く、生活習慣が大きく変わったと感じている方も多いのではないでしょうか。<br>
            その流れに従うように、紙が担う役割も大きく変化してきています。言いかえれば、紙のよい特性が活かせる分野に特化されてきたともいえます。<br>
            今、私たちフクインはその時代の変化に対応すべく、紙に印刷し情報を伝える総合印刷企業から、印刷のプロとしての技術を継承しつつ、デジタルデバイスへの対応を含めた総合情報伝達企業へと歩み出しました。<br>
            時代と共に情報伝達の手段は大きく変化しましたが、情報を加工し、お客様のニーズに応えるという基本は70年前となんら変わりはないと考えています。 </p>
          <p> 今後、更に印刷技術に研きをかけ、より良い印刷サービスを提供し続けること、そしてその土台を活かしたデジタルデータ制作においても、お客様のニーズに応えることを通じ、世の中に貢献してまいります。
            何なりとご相談いただければ幸せです。 </p>
          <p class="signature"> <img src="../assets/images/company/fujiwara.png" alt="藤原俊一"></p>
          <p class="signature"> 株式会社フクイン 代表取締役<br>
            藤原 俊一 </p>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>