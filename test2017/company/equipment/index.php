<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "company-equipment",
		"css" => array(
			"basic",
			"company",
		),
		"title" => "設備一覧｜会社案内｜株式会社フクイン",
		"description" => "株式会社フクインの設備一覧のページです。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Corporate Information</p>
        <p>会社案内</p>
      </div>
      <nav class="breadcrumb">
        <ol>
          <li><a href="/">TOP</a></li>
          <li><a href="/company/">会社案内</a></li>
          <li><a href="./">設備一覧</a></li>
        </ol>
      </nav>
    </div>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>設備一覧</h1>
            <p>Equipment list </p>
          </header>
          <section id="digital">
            <header>
              <h2>デジタル入力・出力</h2>
            </header>
            <dl>
              <dt>測定器</dt>
              <dd>X-Rite SpectroScan 1台<br>
                X-Rite il 2台<br>
                X-Rite iCPlate II 1台 </dd>
              <dt>大判モノクロプリンタ </dt>
              <dd>SII LP-1030MF 1台 </dd>
              <dt>DDCP </dt>
              <dd>大日本スクリーン製造 Lab Proof SE Ver.2.4 2台 <br>
                Canon Pro4000S 2台 </dd>
              <dt>CTP</dt>
              <dd>Kodak Magnus800 1台 </dd>
              <dt>RIP</dt>
              <dd>Kodak PRINERGY Ver.8<br>
                大日本スクリーン製造TrueflowSE Ver.7.30 </dd>
              <dt>CIP4</dt>
              <dd>三菱重工 PPC Server III Ver.3.0.1 </dd>
              <dt>デジタル印刷機 </dt>
              <dd>RICOH Pro C9110 1台<br>
                RICOH Pro C651EX 1台</dd>
            </dl>
          </section>
          <section id="design">
            <header>
              <h2>デザイン・情報処理・組版</h2>
            </header>
            <dl>
              <dt>DTP編集機 </dt>
              <dd>Macintosh、Windows </dd>
              <dt>アプリケーションソフト </dt>
              <dd>InDesign / Illustrator/ Photoshop </dd>
              <dt>自動組版</dt>
              <dd>BookStudio 4台 </dd>
              <dt>ファイルサーバ </dt>
              <dd>Apple XServe 3台<br>
                NewTech CloudyII iX 1台</dd>
            </dl>
          </section>
          <section id="printing">
            <header>
              <h2>印刷</h2>
            </header>
            <dl>
              <dt>印刷機</dt>
              <dd>三菱重工 New ダイヤ304(デジタル色調管理システム対応・菊全5色機) 1台 <br>
                リョービMHIグラフィックテクノロジー DIAMOND V3000R-2/2 <br>
                (デジタル色調管理システム対応・菊全4色機・反転機構付) 1台 <br>
                桜井 オリバー75SD/SDP(菊半寸延び2色機・反転機構付) 1台 </dd>
              <dt>濃度計</dt>
              <dd>X-Rite 508 2台 </dd>
              <dt>インキ自動計量装置</dt>
              <dd>芝橋 レインボーRW-1 1台 <br>
                インキ調色支援システム <br>
                東洋インキ CCMシステム エコマチック21 1台 </dd>
              <dt>断裁機</dt>
              <dd>イトーテック eRC-115DX 1台</dd>
            </dl>
          </section>
          <section id="binding">
            <header>
              <h2>製本</h2>
            </header>
            <dl>
              <dt>製本設備 </dt>
              <dd>断裁機 2台<br>
                折り機 7台<br>
                CCDカメラ・ウエイトチェッカー搭載 無線綴じライン(22鞍・24鞍) 2台<br>
                CCDカメラ搭載 貼込み機(6鞍) 1台<br>
                トライオート(投込み3点対応) 1台<br>
                自動梱包機 1台 </dd>
            </dl>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>