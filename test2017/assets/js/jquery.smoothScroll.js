
jQuery(function($) {
	var key = "smooth-scroll-";

	$('a[href*="#"]').each(function(){
		$(this).attr("href", $(this).attr("href").replace("#", "#" + key));
	});
	
	var smoothScroll = function(hash, t) {

		var h = $("#header nav.global").height() + $("#header nav.global li.current ul").height();
		if (parseInt($("#content").css("margin-right"),10) == 0) {
			var h = $("#header").height();
		}

		if (hash) {
			var targetBody;

			var wst = $(window).scrollTop();
			$(window).scrollTop( wst + 1 );
			if ( $('html').scrollTop() > 0 ) {
				targetBody = $('html');
			} else if ( $('body').scrollTop() > 0 ) {
				targetBody = $('body');
			}

			var targetHash = hash.replace("#" + key, "#");
			var offset = $(targetHash).eq(0).offset();

			targetBody.animate({
				scrollTop: offset.top-h
			}, t,"swing");
		}
		return false;	
	};

	$("body").on("click", 'a[href^=#' + key +']', function() {
		var hash = $(this).attr("href");
		smoothScroll(hash);
	});
	
	$(window).on('load', function() {
		var hash = location.hash;
		if (hash == "#mail") {
			smoothScroll(hash, 0);
		} else {
			smoothScroll(hash, 500);
		}
	});
});