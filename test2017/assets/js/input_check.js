﻿// 入力値チェック
function checkForm(id) {
	var str;

    $( '#' + id ).removeClass( 'correct' );
    $( '#' + id ).removeClass( 'error' );

	if ( $('#' + id).is( ':radio' ) ) {
		str = $( 'input[name=' + id.replace( '1', '' ) + ']' ).val();
	} else {
		str = $( '#' + id ).val();
	}

    if ( $( '#' + id + '-error' ).text() === '' ) {
        if ( str !== '' ) $( '#' + id ).addClass( 'correct' );
    } else {
        $( '#' + id ).addClass( 'error' );
    }
}

// 入力文字変換
function changeInputData(value) {
    value = charToLower(value);
    value = charKanaChange(value);
    value = charHtmlChange(value);
    value = charIzonChange(value);

	return value;
}

// 半角->全角変換
function charToUpper(value) {
    value = value.replace(/[A-Za-z0-9]/g, function (s) {
        return String.fromCharCode(s.charCodeAt(0) + 0xFEE0);
    });
	return value;
}

// 全角->半角変換
function charToLower(value) {
    value = value.replace(/[Ａ-Ｚａ-ｚ０-９]/g, function (s) {
        return String.fromCharCode(s.charCodeAt(0) - 0xFEE0);
    });
	return value;
}

// カナ半角->全角カナ変換
function charKanaChange(value) {
    var Kana1 = new Array("ｶﾞ", "ｷﾞ", "ｸﾞ", "ｹﾞ", "ｺﾞ", "ｻﾞ", "ｼﾞ", "ｽﾞ", "ｾﾞ", "ｿﾞ", "ﾀﾞ", "ﾁﾞ",
        "ﾂﾞ", "ﾃﾞ", "ﾄﾞ", "ﾊﾞ", "ﾋﾞ", "ﾌﾞ", "ﾍﾞ", "ﾎﾞ", "ﾊﾟ", "ﾋﾟ", "ﾌﾟ", "ﾍﾟ", "ﾎﾟ", "ｦ", "ｧ",
        "ｨ", "ｩ", "ｪ", "ｫ", "ｬ", "ｭ", "ｮ", "ｯ", "ｰ", "ｱ", "ｲ", "ｳ", "ｴ", "ｵ", "ｶ", "ｷ", "ｸ", "ｹ",
        "ｺ", "ｻ", "ｼ", "ｽ", "ｾ", "ｿ", "ﾀ", "ﾁ", "ﾂ", "ﾃ", "ﾄ", "ﾅ", "ﾆ", "ﾇ", "ﾈ", "ﾉ", "ﾊ", "ﾋ",
        "ﾌ", "ﾍ", "ﾎ", "ﾏ", "ﾐ", "ﾑ", "ﾒ", "ﾓ", "ﾔ", "ﾕ", "ﾖ", "ﾗ", "ﾘ", "ﾙ", "ﾚ", "ﾛ", "ﾜ", "ﾝ");
    var Kana2 = new Array("ガ", "ギ", "グ", "ゲ", "ゴ", "ザ", "ジ", "ズ", "ゼ", "ゾ", "ダ", "ヂ",
        "ヅ", "デ", "ド", "バ", "ビ", "ブ", "ベ", "ボ", "パ", "ピ", "プ", "ペ", "ポ", "ヲ", "ァ",
        "ィ", "ゥ", "ェ", "ォ", "ャ", "ュ", "ョ", "ッ", "ー", "ア", "イ", "ウ", "エ", "オ", "カ",
        "キ", "ク", "ケ", "コ", "サ", "シ", "ス", "セ", "ソ", "タ", "チ", "ツ", "テ", "ト", "ナ",
        "ニ", "ヌ", "ネ", "ノ", "ハ", "ヒ", "フ", "ヘ", "ホ", "マ", "ミ", "ム", "メ", "モ", "ヤ",
        "ユ", "ヨ", "ラ", "リ", "ル", "レ", "ロ", "ワ", "ン");
    while (value.match(/[ｦ-ﾝ]/)) { //半角カタカナある場合
        for (var i = 0; i < Kana1.length; i++) {
            value = value.replace(Kana1[i], Kana2[i]);
        }
    }
	return value;
}

// HTML記号変換
function charHtmlChange(value) {
    var Moji1 = new Array("<", ">");
    var Moji2 = new Array("＜", "＞");
    while (value.match(/[<>]/)) {
        for (var i = 0; i < Moji1.length; i++) {
            value = value.replace(Moji1[i], Moji2[i]);
        }
    }
	return value;
}

// 機種依存文字変換
function charIzonChange(value) {
    var Moji1 = new Array("Ⅰ","Ⅱ","Ⅲ","Ⅳ","Ⅴ","Ⅵ","Ⅶ","Ⅷ","Ⅸ","Ⅹ","ⅰ","ⅱ","ⅲ","ⅳ","ⅴ","ⅵ","ⅶ","ⅷ","ⅸ","ⅹ","①","②","③","④","⑤","⑥","⑦","⑧","⑨","⑩","⑪","⑫","⑬","⑭","⑮","⑯","⑰","⑱","⑲","⑳","㊤","㊥","㊦","㊧","㊨","㍉","㍍","㌔","㌘","㌧","㌦","㍑","㌫","㌢","㎝","㎏","㎡","㏍","℡","№","㍻","㍼","㍽","㍾","㈱","㈲","㈹");
    var Moji2 = new Array("I","II","III","IV","V","VI","VII","VIII","IX","X","i","ii","iii","iv","v","vi","vii","vii","ix","x","(1)","(2)","(3)","(4)","(5)","(6)","(7)","(8)","(9)","(10)","(11)","(12)","(13)","(14)","(15)","(16)","(17)","(18)","(19)","(20)","(上)","(中)","(下)","(左)","(右）","ミリ","メートル","キロ","グラム","トン","ドル","リットル","パーセント","センチ","cm","kg","m2","K.K.","TEL","No.","平成","昭和","大正","明治","(株)","(有)","(代)");
    for (var i = 0; i < Moji1.length; i++) {
        value = value.replace(Moji1[i], Moji2[i]);
    }
	return value;
}


// ひらがな->カタカナ変換
function charHiraganaToKatagana(src) {
    return src.replace(/[\u3041-\u3096]/g, function (match) {
        var chr = match.charCodeAt(0) + 0x60;
        return String.fromCharCode(chr);
    });
}
