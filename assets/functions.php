<?php
	function get_header($params = array()) {
?>
<!doctype html>
<html><head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width,initial-scale=1.0,user-scalable=no">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="format-detection" content="telephone=no">
<title><?php echo $params['title'] ?></title>
<meta name="description" content="<?php echo $params['description'] ?>">
<meta property="og:title" content="<?php echo $params['title'] ?>">
<meta property="og:type" content="<?php if ($params['body_class'] == "home"): ?>website<?php else: ?>article<?php endif;?>">
<meta property="og:image" content="/assets/images/common/og_image.png">
<meta property="og:site_name" content="株式会社フクイン">
<meta property="og:description" content="<?php echo $params['description'] ?>">
<link rel="shortcut icon" href="/assets/icons/favicon.ico">
<link rel="icon" type="image/x-icon" href="/assets/icons/favicon.ico">
<link rel="apple-touch-icon" href="/assets/icons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="192x192"  href="/assets/icons/android-chrome-192x192.png">
<link rel="icon" type="image/png" sizes="256x256"  href="/assets/icons/android-chrome-256x256.png">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/icons/favicon-16x16.png">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/icons/favicon-32x32.png">
<link rel="manifest" href="/assets/icons/manifest.json">
<meta name="theme-color" content="#ffffff">
<meta name="msapplication-config" content="/assets/icons/browserconfig.xml">
<meta name="msapplication-TileColor" content="#da532c">
<meta name="msapplication-TileImage" content="/assets/icons/mstile-150x150.png">
<link href="https://fonts.googleapis.com/css?family=Prata|Roboto" rel="stylesheet">
<link rel="stylesheet" href="/assets/css/reset.css">
<link rel="stylesheet" href="/assets/css/common.css">
<?php
		foreach((array)@$params['css'] as $v):
?>
<link rel="stylesheet" href="/assets/css/<? echo $v ?>.css">
<?php
		endforeach;
?>
<!--[if lt IE 9]>
<script src="/assets/js/html5.js"></script>
<![endif]-->
<script src="/assets/js/jquery-1.11.2.js"></script>
<script src="/assets/js/jquery.smoothScroll.js"></script>
<script src="/assets/js/common.js"></script>
<?php
		foreach((array)@$params['js'] as $v):
?>
<script src="<? echo $v ?>"></script>
<?php
		endforeach;
?>
</head>
<body class="<?php echo $params['body_class'] ?>">
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TFCVJ2"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TFCVJ2');</script>
<!-- End Google Tag Manager -->
<div id="pagetop">
  <header id="header">
    <?php if ($params['body_class'] == "home") : ?>
    <h1 class="logo"><a href="/">FUKUIN</a></h1>
    <?php else: ?>
    <p class="logo"><a href="/">FUKUIN</a></p>
    <?php endif; ?>
		<p>株式会社フクイン<br>
      総合印刷会社</p>
    <p class="menu"><a href=""><span>MENU</span></a></p>
    <nav class="contact">
      <p>CONTACT</p>
      <ul>
        <li class="tel">
          <p>03-3946-2281 </p>
          <p>平日 <span>9:00-18:00</span></p>
          <p>法人<span>・</span>団体専用ダイヤル</p>
        </li>
        <li class="web"><a href="/contact/"> WEBからの<br>
          お問い合わせ</a></li>
        <li class="web"><a href="/recruit/contact/"> 採用に関する<br>
        お問い合わせ</a></li>
      </ul>
    </nav>
    <p class="anniversary"><img src="/assets/images/common/70thlogo_black.svg" alt="70th anniversary year of the company’s founding"></p>
    <nav class="global">
      <ul>
				<li class="home"><a href="/"><span>TOP</span></a>
				<li class="company"><a href="/company/"><span>会社案内</span></a>
          <ul>
            <li><a href="/company/">社長挨拶</a></li>
            <li class="philosophy"><a href="/company/philosophy/">理念</a></li>
            <li class="profile"><a href="/company/profile/">企業概要</a></li>
            <li class="development"><a href="/company/development/">沿革</a></li>
            <li class="history"><a href="/company/history/">歴史</a></li>
            <li class="equipment"><a href="/company/equipment/">設備一覧</a></li>
            <li class="access"><a href="/company/access/">アクセス</a></li>
            <li class="csr"><a href="/company/csr/">CSR</a></li>
            <li class="privacy"><a href="/company/privacy/">プライバシーポリシー</a></li>
          </ul>
        </li>
				<li class="technique"><a href="/technique/"><span>技 術</span></a>
          <ul>
            <li><a href="/technique/">組版・制作</a></li>
            <li class="proof"><a href="/technique/proof/">色校正</a></li>
            <li class="printing"><a href="/technique/printing/">印刷</a></li>
            <li class="process"><a href="/technique/process/">製本・加工</a></li>
            <li class="trobleshooting"><a href="/technique/trobleshooting/">トラブル解決</a></li>
            <li class="case"><a href="/technique/case/">製品事例</a></li>
          </ul>
        </li>
				<li class="service"><a href="/service/"><span>サービス</span></a>
          <ul>
            <li><a href="/service/">フルサービス</a></li>
            <li class="theater"><a href="/service/theater/">映画館プロモーション</a></li>
            <li class="web"><a href="/service/web/">Webプロモーション</a></li>
          </ul>
        </li>
<?php /*				
				<li class="works"><a href="/works/"><span>実 績</span></a>
        </li>
				<li class="fukuinfo"><a href="/fukuinfo/"><span>FUKUINFO</span></a>
        </li>
			*/
?>																					
				<li class="recruit"><a href="/recruit/"><span>採用情報</span></a>
          <ul>
            <li class=""><a href="/recruit/">採用情報</a></li>
            <li class="fresh"><a href="/recruit/fresh/">募集要項（新卒）</a></li>
            <li class="career"><a href="/recruit/career/">募集要項（中途）</a></li>
            <li class="contact"><a href="/recruit/contact/">採用に関するお問い合わせ</a></li>
          </ul>
     </li>
      </ul>
    </nav>
    <nav id="menu">
      <ul class="primary">
			</ul>
      <ul class="secondary">
        <li class="contact"><a href="/contact/">お問い合わせ</a></li>
        <li class="tel">
          <p>法人<span>・</span>団体専用ダイヤル</p>
          <p>03-3946-2281</p>
          <p>平日 <span>9:00-18:00</span></p>
        </li>
        <li class="sitemap"><a href="/sitemap/">サイトマップ</a></li>
      </ul>
    </nav>
  </header>
  <div id="content">
<?php
	}

	function get_footer($params = array()) {
?>
  </div>
  <footer id="footer">
    <div class="inner">
      <p class="pagetop"><a href="#pagetop">PAGETOP</a></p>
      <div id="footer-01">
        <nav class="contact">
          <p> CONTACT </p>
          <ul>
            <li class="tel">
              <p>法人・団体専用ダイヤル </p>
              <p>03-3946-2281</p>
              <p>平日 <span>9:00-18:00</span></p>
            </li>
            <li class="web"><a href="/contact/">WEBからのお問い合わせ</a></li>
          </ul>
        </nav>
        <div class="information">
          <p class="fukuin"> 株式会社フクイン </p>
          <p class="address"> 〒112-0013 <br>
            東京都文京区音羽1-23-3<br>
            TEL : 03-3946-2281 </p>
          <p class="copyright"> © FUKUIN CO., LTD. </p>
        </div>
      </div>
      <div id="footer-02">
        <nav class="menu">
          <ul>
            <li><a href="/company/">会社案内</a></li>
            <li><a href="/technique/">技術</a></li>
            <li><a href="/service/">サービス</a></li>
<?php /*            
            <li><a href="/works/">実績</a></li>
            <li><a href="/fukuinfo/">FUKUINFO</a></li>
			*/
?>		
            <li><a href="/recruit/">採用情報</a></li>
            <li><a href="/sitemap/">サイトマップ</a></li>
          </ul>
        </nav>
      </div>
    </div>
  </footer>
</div>
</body>
</html>
<?php
}