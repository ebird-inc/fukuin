
/*
	タブレット対応
*/

$(function() {
	if(navigator.userAgent.indexOf('iPhone') > 0 || navigator.userAgent.indexOf('iPod') > 0 || (navigator.userAgent.indexOf('Android') > 0 && navigator.userAgent.indexOf('Mobile') > 0)){
		$("meta[name='viewport']").remove();
		$("head").append('<meta name="viewport" content="width=device-width,user-scalable=yes">');             
	} else {
		$("meta[name='viewport']").remove();
		$("head").append('<meta name="viewport" content="width=1020px,user-scalable=auto">');
	}
});

/*
	スライドショー
*/

$(document).ready(function() {
	if ($("body.home").get(0)) {
		$("#visual").height($(window).height());		
		$(".visual").width($(window).width());		
		$(".visual").height($(window).height());		
	
		$(window).resize(function(){
			$("#visual").height($(window).height());		
			$(".visual").width($(window).width());		
			$(".visual").height($(window).height());		
		});

		$(window).load(function() {	
			var animateVisual = function() {
				var index = $("#visual ul li.is-active").index("#visual ul li");
				$("#visual .is-show").removeClass("is-show");
				$("#visual .is-active").addClass("is-show");
				if (index >= $("#visual ul li").length-1) {
					nth = 1;					
				} else if (index < 0) {
					nth = 1;
				} else {
					nth = index+2;
				}
				$("#visual ul li").removeClass("is-active");
				$("#visual ul li:nth-child(" + nth + ")").addClass("is-active");
				setTimeout(animateVisual, 5000);
			}
			animateVisual();
		});		
	}
});

/*
	メニュー
*/

$(document).ready(function() {
	var arr = window.location.href.split("/");
	var p = arr[3];
	var c = arr[4];
	if (!p) {
		p = "home";
	}
	$("#header .global li." + p).addClass("current");

	if (c) {
		$("#header .global li." + p + " li." + c).addClass("current");
	} else {
		$("#header .global li." + p + " li:first-child").addClass("current");
	}

	$("#header .global > ul > li").hover(function() {
		$("#header .global > ul > li.current").removeClass("current");
		$(this).addClass("current");
	}, function() {
		$("#header .global > ul > li").removeClass("current");

		if (p) {
			$("#header .global li." + p).addClass("current");
		} else {
			$("#header .global li.home" + p).addClass("current");
		}

		if (c) {
			$("#header .global li." + p + " li." + c).addClass("current");
		} else {
			$("#header .global li." + p + " li:first-child").addClass("current");
		}
	
	
	});
	
	
	$("#header #menu .primary").html($("#header .global ul").html());
	$("#header #menu .primary > li").each(function() {
		if ($("ul", this).get(0)) {
			$(this).addClass("accordion");
		}
	});
	
	$("#header .menu").click(function() {
		$("body").toggleClass("is-menu-opened");
		return false;
	});

	$(document).on("click", "#header #menu .accordion > a", function() {
		var li = $(this).parent(".accordion");
		if (li.hasClass("is-opened")) {
			li.removeClass("is-opened");
			li.children("ul").slideUp();
		} else {
			li.addClass("is-opened");
			li.children("ul").slideDown();
		}
		return false;
	});

});

/*
	ホーム：グロナビ位置設定
*/

$(document).ready(function() {
	if ($("body.home").get(0)) {
		var setHeaderPos = function() {
			var y = window.pageYOffset;
			if (y > $(window).height() - $("#header nav.global").height()) {
				$("#header nav.global").css("top", 0);
				$("body").addClass("is-global-fixed");
			} else {
				$("#header nav.global").css("top", $(window).height() - $("#header nav.global").height() - y + "px");
				$("body").removeClass("is-global-fixed");
			}			
		}

		setHeaderPos();
	
		$(window).resize(function(){
			setHeaderPos();
		});

		$(window).scroll(function(){
			setHeaderPos();
		});
	}
});

/*
	ホーム：スクロールインアニメーション表示
*/

$(document).ready(function() {
	if ($("body.home").get(0)) {
		var scrollin = function() {			
			$(".scrollin").each(function(){
				var imgPos = $(this).offset().top;   
				var scroll = $(window).scrollTop();
				var windowHeight = $(window).height();
				if (scroll > imgPos - windowHeight + windowHeight/5) {
					$(this).addClass("is-show");
				} else {
				}
			}); 
		};	

		$(window).load(function() {
			scrollin();
		});

		$(window).resize(function() {
			scrollin();
		});

		$(window).scroll(function (){
			scrollin();
		});
	}
});


/*
	ホーム：ノウハウサイズ調整
*/

$(document).ready(function() {
	if ($("body.home").get(0)) {
		var setKnowhowSize = function() {
			$("#knowhow .site").css("height", "auto");
			$("#knowhow ul li").css("height", "auto");
			var max = 0;
			$("#knowhow ul li").each(function() {
				if ($(this).height() > max) {
					max = $(this).height();
				}		
			});
			$("#knowhow ul li").css("height", max + "px");
			$("#knowhow .site").css("height", $("#knowhow ul").height()-1 + "px");

			/*
			if () {
			}
			$("#knowhow .site").css("height", max * 2);
*/
		}

		setKnowhowSize();
	
		$(window).resize(function(){
			setKnowhowSize();
		});
	}
});


/*
	お問い合わせ：入力チェック
*/

$(document).ready(function() {
	if ($("body.contact").get(0) || $("body.recruit-contact").get(0)) {
		var validateForms = function(t) {
			t.removeClass("success");
			t.removeClass("error");

			if (t.prop("required")) {
				if (t.val() == "") {
					t.addClass("error");	
				}
			}
			
			if (t.data("validate-min-length")) {
				if (t.data("validate-min-length") > t.val().length) {
					t.addClass("error");
				}
			}

			if (t.data("validate-max-length")) {
				if (t.data("validate-max-length") < t.val().length) {
					t.addClass("error");
				}
			}

			if (t.data("validate-numeric") != undefined) {
				if(!t.val().match(/^[0-9]+$/)){
					t.addClass("error");	
				}
			}

			if (t.data("validate-katakana") != undefined) {
				if(!t.val().match(/^[ァ-ロワヲンー 　\r\n\t]*$/)){
					t.addClass("error");	
				}
			}			
			
			if (t.attr("type") == "email") {
				if (!t.val().match(/^[-a-z0-9~!$%^&*_=+}{\'?]+(\.[-a-z0-9~!$%^&*_=+}{\'?]+)*@([a-z0-9_][-a-z0-9_]*(\.[-a-z0-9_]+)*\.(aero|arpa|biz|com|coop|edu|gov|info|int|mil|museum|name|net|org|pro|travel|mobi|[a-z][a-z])|([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}))(:[0-9]{1,5})?$/i)) {
					t.addClass("error");	
				}
			}

			if (!t.hasClass("error") && t.val() != "") {
				t.addClass("success");
			}
		};
		
		$('form input, form textarea').on('input', function () {
			validateForms($(this));
		});				
		$('form input, form textarea').focusout(function() {
			validateForms($(this));
		});

		$('form input, form textarea').each(function() {
//			validateForms($(this));
		});
	
	}
});