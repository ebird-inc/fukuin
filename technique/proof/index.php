<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "technique-proof",
		"css" => array(
			"basic",
			"technique",
		),
		"title" => "色校正｜技術｜株式会社フクイン",
		"description" => "株式会社フクインの技術、色校正のページです。実際の印刷を行う前に、色の確認やデータの不具合などを確認するために行う色校正には、大きく分けて3種類あります。フクインでは、印刷製品の用途や目的によって適切な手段の色校正をご提案しています。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Know-How</p>
        <p>フクインの技術</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/technique/">技術</a></li>
        <li><a href="./">色校正</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>色校正</h1>
            <p>Color Proof</p>
          </header>
          <section id="proof">
            <header>
              <h2>色校正</h2>
              <p>色と文字を同時に確認</p>
            </header>
            <p>テキストの変更や修正、写真の調子やタイトルの色調など、文字と色を同時に確認できる色校正。フクインには3種の色校正があり、校正の段階や全体の予算、納期などから最適な色校正をご提案します</p>
          </section>
          <section id="machine">
            <header>
              <h2>本機校正</h2>
            </header>
            <p> 本機校正は実際に印刷するものと同じ紙、同じインキ、同じ印刷機を使用して少部数だけ印刷を行う校正方法です。 </p>
            <figure><img src="../../assets/images/technique/proof/machine_img.png" alt=""></figure>
            <section>
              <h3 class="h4">本機校正のポイント</h3>
              <p> 本生産の印刷機で色校正を印刷します。コストは他の校正と比べて割高となりますが、表裏で印刷するなど、印刷製品の仕上がりに一番近づく校正方法です。写真集や自動車などの製品カタログなど、忠実な色味の再現が必要となるものは本機校正による色調チェックが適しています。 </p>
            </section>
          </section>
          <section id="paper">
            <header>
              <h2>本紙校正（平台校正）</h2>
            </header>
            <p>本紙校正（平台色校正）は実際に印刷するものと同じ紙、同じインキを使用し、校正専用の印刷機（平台校正機）で印刷します。 </p>
            <figure><img src="../../assets/images/technique/proof/paper_img.png" alt=""></figure>
            <section>
              <h3 class="h4">本紙校正（平台色校正）のポイント</h3>
              <p>印刷本機と平台校正機では印刷の仕組みが違うので、若干の色差が発生する場合もありますが、用紙予備枚数が少ないので、本機校正より低コストで実際の用紙の風合いやインキが乗った状態を確認することができます。書籍付物など用紙が高いものなどに適しています。</p>
            </section>
          </section>
          <section id="ddcp">
            <header>
              <h2>DDCP（簡易校正）</h2>
            </header>
            <p>カラー印刷の色校正手段です。インクジェットプリンターを使用するものが主流で、専用紙タイプのもの、実際の用紙を使用するものがあります。出力時のレイアウトは自由に設定できるため、カラープリンタなどの事前校正と同じ見開きでの出力も可能で、比較し易く、赤字の入れやすい出校も可能です。</p>
            <figure><img src="../../assets/images/technique/proof/ddcp_img.png" alt=""></figure>
            <section id="ddcp-01">
              <h3 class="h4">DDCPのポイント</h3>
              <p>低コスト、短時間での色校正が可能となります。専用紙タイプでも、コートやマット紙などの用紙種別のプロファイルデータをライブラリー化し、シミュレーションして出力するため、色調再現性と安定性に優れています。風合いや質感を持つファンシーペーパーを使用する案件には、実際の用紙を使う本紙タイプをご用意しています。</p>
            </section>
            <section id="ddcp-02">
              <h3 class="h4">DDCPのポイント、プロファイル</h3>
              <p>DDCPで実際の印刷に近い色調を再現するためには、使用するプロファイルデータの正確性がポイントとなります。フクインでは自社のプロファイルデータをライブラリー化。長年の蓄積により、より正確な用紙のシミュレーションを可能にしています</p>
              <div class="row">
                <div class="col">
                  <figure><img src="../../assets/images/technique/proof/ddcp_02_img_01.png" alt="">
                    <figcaption>本機よりの刷出しを分光測色機にセット</figcaption>
                  </figure>
                </div>
                <div class="col">
                  <figure><img src="../../assets/images/technique/proof/ddcp_02_img_02.png" alt="">
                    <figcaption>ヘッドが紙上を走り測色していきます</figcaption>
                  </figure>
                </div>
                <div class="col">
                  <figure><img src="../../assets/images/technique/proof/ddcp_02_img_03.png" alt="">
                    <figcaption>読み込まれたデータをモニターに表示</figcaption>
                  </figure>
                </div>
                <div class="col">
                  <figure><img src="../../assets/images/technique/proof/ddcp_02_img_04.png" alt="">
                    <figcaption>ライブラリー化されたデータをDDCPに適用</figcaption>
                  </figure>
                </div>
              </div>
              <p> <strong>※DDCP</strong><br>
                DDCPとは「ダイレクト・デジタル・カラー・プルーフィング」の略。校正紙を校正専用機で刷らずに専用紙に直接出力するカラー出力システム、または、それを使った校正方法の呼称です。 </p>
            </section>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>