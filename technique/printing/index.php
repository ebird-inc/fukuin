<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "technique-printing",
		"css" => array(
			"basic",
			"technique",
		),
		"title" => "印刷｜技術｜株式会社フクイン",
		"description" => "株式会社フクインの技術、印刷のページです。フクインの印刷は、0.01mmの再現性をもった高精細印刷を可能としています。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Know-How</p>
        <p>フクインの技術</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/technique/">技術</a></li>
        <li><a href="./">印刷</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>印刷</h1>
            <p>Printing</p>
          </header>
          <section id="hd">
            <header>
              <h2>多彩な印刷対応力</h2>
              <p>４色+αのカタログから、書籍の付き物まで</p>
            </header>
            <section id="hd-01">
              <p>フクインはお客様の多様なニーズに応えられるよう、様々な印刷物にスピーディに対応できる印刷機を取り揃えています。プロセス４色に加え、艶感を表現するグロスニスやマットニス、目を惹くような特色などの効果によって、付加価値の高い印刷物をワンラインで印刷できます。また、反転機構を備えた４色機では、プロセス４色のポスターを印刷した後、同じ印刷機で２色本文の両面印刷を行えるなど、日程や納期の変更に対応する生産体制も確保しています。<br>
                他にも、印刷から加工までできるオンデマンド印刷機を保有しており、少部数の案件にも対応しています。</p>
              <figure><img src="../../assets/images/technique/printing/machine_img.png" alt=""></figure>
            </section>
          　<section>
            <header>
              <h2>特色・カラーマネジメント</h2>
              <p>お客様のこだわりを実現する印刷の管理能力</p>
            </header>
            <section id="hd-01">
              <h3 class="h4">特色インキは社内でつくる</h3>
              <p>鮮やかな色、パステル調の色などプロセス４色では表現できない色は特色印刷で対応します。書籍であれば、表紙や帯などの付物、実用書の本文で。また、企業ロゴなど厳密な色再現が要求されるカタログや会社案内など。フクインでは調色機により自社で特色をつくることで迅速な対応が可能になるだけでなく、「DICのこの番号に合わせて刷り上がりを鮮やかにしたい」、「銀を少し青くしたい」、「この現物と同じ色で」などといったご要望にもお応えしています。</p>
              <figure><img src="../../assets/images/technique/printing/hd_img.png" alt=""></figure>
              <h3 class="h4">色調管理システム</h3>
              <p>フクインの主要な印刷機には色調管理システムを装備。インキ濃度、ドットゲインなどの数値管理をベースに、印刷オペレーターが安定した印刷再現を可能にしています。特色印刷専用機にも色調管理システムを装備し、書籍付物などの再版でも安定した再現性を実現しています。</p>
            </section>
            </section>
          　<section>
            <header>
              <h2>高精細印刷</h2>
              <p>0.01mmの再現性。フクインの高精細印刷</p>
            </header>
          </section>
            <section id="pre-01">
              <h3 class="h4">実績のあるFMスクリーン印刷</h3>
              <p>フクインは、CTP技術の導入直後から高精細印刷への取り組みを開始。FMスクリーンに焦点を絞りテストを重ね、写真集での実践をすすめてきました。モノクロ写真集では通常のダブルトーンではなく、高濃度墨＋ニスの2色刷で行い、視覚的に濃度域の広い、ダブルトーンに匹敵する再現を可能にしました。広演色プロセスインキとの組み合わせで、色再現領域をさらに広げた写真集も手がけています。 </p> 
                <div class="row">
                  <div class="col">
                    <figure><img src="../../assets/images/technique/printing/hd_04_img.jpg" alt="">
                    </figure>
                  </div>
                </div>
                  <p>ドットサイズは均一のままドット個数の変調により階調を表現するFMスクリーンのアミ点。FMスクリーンはCTPのメリットを活かし、データにより近いトーンの再現を可能にしています。</p>
            </section>
          <section id="reproducibility">
            <header>
              <h2>時代に先駆けて導入したCTP技術</h2>
            </header>
            <p>CTP（Computer To Plate）技術は旧来の版の現像方法を簡略化させ、下版工程での日程短縮を実現させました。フクインは1996年に国内でも先駆けてCTPを導入。 DTP環境の充実と併せてCTP技術を向上させてきました。<br>
              目視によるチェックと併せた検査手法を確立させ、スピードと安全性を両立。それぞれの印刷や製本工程などに合ったカラーバーやトンボ※の開発。「より精度の高い版を」、「より早く印刷現場へ」、「よりお客様のイメージする仕上りで」高品質・短納期を可能にしています。</p>
              <p class="note">※絵柄以外に印刷用紙の周囲などに印字する検査用マークのこと</p>
            <figure><img src="../../assets/images/technique/printing/imagephoto-s_15.png" alt=""></figure>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>