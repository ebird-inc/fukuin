<?php
	include $_SERVER['DOCUMENT_ROOT'] . "/assets/functions.php";

	get_header_html(array(
		"body_class" => "technique-case",
		"css" => array(
			"basic",
			"technique",
		),
		"title" => "製品事例｜技術｜株式会社フクイン",
		"description" => "株式会社フクインの技術、製品事例のページです。フクインの製品事例として、カタログ、パンフレット、書籍、取扱説明書、パッケージなどフクインの製品事例を紹介します。",
	));
?>
    <div id="visual">
      <div class="site">
        <p>Know-How</p>
        <p>フクインの技術</p>
      </div>
    </div>
    <nav class="breadcrumb">
      <ol>
        <li><a href="/">TOP</a></li>
        <li><a href="/technique/">技術</a></li>
        <li><a href="./">製品事例</a></li>
      </ol>
    </nav>
    <main>
      <div id="wrapper">
        <div class="site">
          <header>
            <h1>製品事例</h1>
            <p>Product Introduction</p>
          </header>
          <section id="product">
            <header>
              <h2>フクインのものづくり</h2>
              <p>思いやこだわりをカタチに</p>
            </header>
            <section>
              <h3 class="h4">カタログ</h3>
              <figure><img src="../../assets/images/technique/case/carro_image1.png" alt=""></figure>
              <p>商品には企画者や製作者の思いやこだわりが必ず入っています。<br>
                その思いやこだわりを印刷でどのように表現するか。<br>
                用紙とインク、加工技術を用いて、どのように表現するかで、<br>
                カタログやパンフレットを手に取った人に伝えられる印象は変化します。 <br>
                商品の素材感を出すのか、備わっている機能を表現するのか。<br>
                思いをカタチにするために、インキ濃度を調整してキレのある表現を狙い、<br>
                特殊加工を用いて人間の五感をくすぐる仕上りを狙う事もあります。</p>
              <p>その他にもやわらかい、優しい雰囲気を伝える。<br>
                高級感や、インパクトのあるイメージを与える。<br>
                革新的な技術をアピールする。<br>
                これまで培ってきた70年のノウハウを引き出しながらカタログと向き合います。</p>
              </p>
            </section>
          </section>
          <section id="book">
              <h3 class="h4">書籍</h3>
            <figure><img src="../../assets/images/technique/case/book_img.png" alt="">
            </figure>
              <p>フクインは企画から配本・販促まで、本づくりのすべてにお応えします。</p>
              <p>用紙の取り都合からご提案できる変則折や変形サイズ。<br>
                80以上ある用紙銘柄から、ご要望に応じた本文用紙をご提案。<br>
                辞典でも実績のある、InDesignを駆使した組版。<br>
                写真集の経験技術を、書籍のモノクロ写真再現にも応用。<br>
                1色・2色の実用書本文でも高精細印刷で図版をより判り易く。<br>
                カバーを彩る様々な表面加工にも対応。<br>
                壊れにくい本、開きの良い本、大量ページの本、上製本・並製本を問わず対応できる製本。<br>
                書店はもちろん、種々の商業施設での販促もご提案。</p>
            <p>フクインは本づくりの自社一貫生産の強みを活かし、著者・編集者の方々とのコミュニケーションを重ねて、あらゆる出版物のご要望にお応えします。</p>
          </section>
          <section id="package">
              <h3 class="h4">パッケージ</h3>
            <figure class="right_mt80"><img src="../../assets/images/technique/case/package_img.png" alt=""></figure>
            <p>機能的なパッケージには、その商品の保護構造はもとより、多様化した消費者ニーズに応えるべく、消費者の使いやすさに配慮した加工技術や、目を惹くデザインやアイキャッチ、ブランドイメージの浸透などが要求されます。<br>
              フクインでは高級板紙、ダンボール素材、プラスティック素材など様々な材質に、UV印刷やPP&PET加工、箔押しなどの加工技術を加え、商品を使用するシチュエーションやスタイルに合わせたご提案をいたします。<br>
              商品開発の段階からご相談いただければ、ダミー箱の作成も含め、的確なディレクションや品質とコストバランスのアドバイスもいたします。</p>
          </section>
        </div>
      </div>
    </main>
<?php
	get_footer_html();
?>